<?php

namespace frontend\controllers;

use Yii;
use common\models\People;
use common\models\PeopleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\web\UploadedFile;

/**
 * PeopleController implements the CRUD actions for People model.
 */
class PeopleController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                    'recover' => ['POST'],
                    'delete-permanent' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all People models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PeopleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single People model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new People model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new People();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->ID]);
            //return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model, 'rolesArray' => $rolesArray
            ]);
        }
    }

    /**
     * Updates an existing People model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelStaff = $this->findModel($id);
        $model->setScenario('admin-update');
        $roles = \common\models\Roles::find()->where(['DELETED' => 0])->asArray()->all();
        $rolesArray = \yii\helpers\ArrayHelper::map($roles, 'ID', 'NAME');

        if ($model->load(Yii::$app->request->post())) {
            $flag = true;
            //get instance of uploaded file
            $model->PROFILE_PIC_FILE = UploadedFile::getInstance($model, 'PROFILE_PIC_FILE');
            if (!empty($model->PROFILE_PIC_FILE)) {
                $imageName = $model->USERNAME;
                $imageUrl = \Yii::$app->params['profilePicPath'] . $imageName . '_' . time() . '.' . $model->PROFILE_PIC_FILE->extension;
                if ($model->PROFILE_PIC_FILE->extension == 'jpg' || $model->PROFILE_PIC_FILE->extension == 'png') {
                    //upload file to server
                    $model->PROFILE_PIC_FILE->saveAs($imageUrl);
                    //save path in db
                    $model->PROFILE_PIC = $imageUrl;
                } else {
                    $flag = false;
                }
            }
            if ($model->isNewRecord || (!$model->isNewRecord && $model->PASSWORD_TEMP && $model->REPASSWORD_TEMP)) {
                $model->setPassword($model->PASSWORD_TEMP);
                $model->generateAuthKey();
                $model->generatePasswordResetToken();
                \Yii::$app->session->setFlash('password', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Kata Laluan Berubah!',
                    'message' => ' Data berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
//            return $this->redirect(['view', 'id' => $model->ID]);
//            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model, 'rolesArray' => $rolesArray
            ]);
        }
    }

    /**
     * Deletes an existing People model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->DELETED = '1';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }

        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $model = $this->findModel($id);
        $model->DELETED = '0';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(['index']);
    }

    public function actionDeletePermanent($id) {
        $existTransactions = \common\models\Transactions::find()
                ->where(['CREATED_BY' => $id])
                ->orWhere(['UPDATED_BY' => $id])
                ->count();
        $existOrders = \common\models\Orders::find()
                ->where(['CREATED_BY' => $id])
                ->orWhere(['UPDATED_BY' => $id])
                ->orWhere(['APPROVED_BY' => $id])
                ->count();
        $existInventoriesCheckin = \common\models\InventoriesCheckIn::find()
                ->where(['CREATED_BY' => $id])
                ->orWhere(['UPDATED_BY' => $id])
                ->orWhere(['APPROVED_BY' => $id])
                ->count();
        $existItems = \common\models\InventoryItems::find()
                ->where(['CREATED_BY' => $id])
                ->orWhere(['UPDATED_BY' => $id])
                ->count();
        $existOrderItems = \common\models\OrderItems::find()
                ->where(['CREATED_BY' => $id])
                ->orWhere(['UPDATED_BY' => $id])
                ->count();
        $existCategories = \common\models\Categories::find()
                ->where(['CREATED_BY' => $id])
                ->orWhere(['UPDATED_BY' => $id])
                ->count();
        $existRoles = \common\models\Roles::find()
                ->where(['CREATED_BY' => $id])
                ->orWhere(['UPDATED_BY' => $id])
                ->count();
        $existVehicleList = \common\models\VehicleList::find()
                ->where(['CREATED_BY' => $id])
                ->orWhere(['UPDATED_BY' => $id])
                ->count();
        $existVendors = \common\models\Vendors::find()
                ->where(['CREATED_BY' => $id])
                ->orWhere(['UPDATED_BY' => $id])
                ->count();
        $existActivityLogs = \common\models\ActivityLogs::find()
                ->where(['USER_ID' => $id])
                ->count();
        $existCount = $existActivityLogs + $existVendors + $existVehicleList + $existRoles + $existCategories + $existOrderItems + $existOrders + $existItems + $existInventoriesCheckin + $existTransactions;
        if ($existCount > 0) {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Pengguna ini tidak boleh dibuang secara kekal. Terdapat ' . $existCount . ' rekod keatas pengunna.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            $this->findModel($id)->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the People model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return People the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = People::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModelStaff($staffNo) {
        if (($model = \common\models\MpspStaff::findOne(['STAFF_NO'=>$staffNo])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
