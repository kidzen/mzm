<?php

namespace frontend\controllers;

use Yii;
use common\models\Inventories;
use common\models\InventoryItemsSearch;
use common\models\InventoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\base\Exception;

/**
 * InventoriesController implements the CRUD actions for Inventories model.
 */
class InventoriesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                    'delete-permanent' => ['POST'],
                    'recover' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Inventories models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new InventoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintBarcode($id,$printChild = 0) {
        Inventories::printInventoryBarcode($id,$printChild);
    }
    public function actionPrintAllSku($id,$printChild = 1) {
        Inventories::printAllSku($id,$printChild);
    }

    public function actionTransactions($id) {
        $inventory = $this->findInventory($id);
        $transactions = \common\models\Transactions::find()
//                ->joinWith('inventory')
//                ->joinWith('inventoriesCheckIn')
//                ->joinWith('inventoriesCheckOut')
//                ->where(['INVENTORIES.ID'=>$id])
//                ->asArray()
                ->all();
//        var_dump($transactions);die();
        return $this->render('transactions', [
                    'transactions' => $transactions,
                    'inventory' => $inventory,
        ]);
    }

    public function actionItemList($id) {

        $searchModel = new InventoryItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere((['INVENTORIES.ID' => $id, 'INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' => NULL
            , 'INVENTORY_ITEMS.DELETED' => 0, 'INVENTORIES.DELETED' => 0, 'INVENTORIES_CHECKIN.DELETED' => 0]));

        return $this->render('item-list', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionItemTransactions($id) {
        $inventory = $this->findInventory($id);
//         var_dump(\common\models\TransactionsAll::find()->where(['INVENTORY_ID'=>$id])->all());die();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\TransactionsAll::find()
                   // ->with('checkBy.mpspProfile')
//                    ->with('checkBy.mpspProfile')
                    ->where(['INVENTORY_ID' => $id]),
        ]);
        $transactions = $dataProvider->models;

        return $this->render('item-transactions', [
                    'dataProvider' => $dataProvider,
                    'transactions' => $transactions,
                    'inventory' => $inventory,
        ]);
    }

    public function actionPdfItemTransactions($id) {
        $inventory = $this->findInventory($id);die();
//         var_dump(\common\models\TransactionsAll::find()->where(['INVENTORY_ID'=>$id])->all());die();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\TransactionsAll::find()->where(['INVENTORY_ID' => $id]),
        ]);
        $transactions = $dataProvider->models;
        var_dump($transactions);die();
//        return $this->renderPartial('item-transactions', [
//            'dataProvider' => $dataProvider,
//            'transactions' => $transactions,
//            'inventory' => $inventory,
//        ]);
        $content = $this->renderPartial('_item-transactions', [
            'dataProvider' => $dataProvider,
            'transactions' => $transactions,
            'inventory' => $inventory,
        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
            'options' => [
                'title' => 'Kad Petak ' . $inventory->CODE_NO,
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
//            'methods' => [
//                'SetHeader' => ['Generated By: MPSP Estor Application||Generated On: ' . date("r")],
//                'SetFooter' => ['|Page {PAGENO}|'],
//            ]
        ]);
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render();
    }

    /**
     * Displays a single Inventories model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new Inventories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Inventories();
        $categories = \common\models\Categories::find()->asArray()->all();
        $categoryArray = \yii\helpers\ArrayHelper::map($categories, 'ID', 'NAME');


        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->ID]);
            //return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'categoryArray' => $categoryArray,
            ]);
        }
    }

    /**
     * Updates an existing Inventories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $categories = \common\models\Categories::find()->asArray()->all();
        $categoryArray = \yii\helpers\ArrayHelper::map($categories, 'ID', 'NAME');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->ID]);
            //return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'categoryArray' => $categoryArray,
            ]);
        }
    }

    /**
     * Deletes an existing Inventories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->DELETED = '1';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(Yii::$app->request->referrer);
//        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $model = $this->findModel($id);
        $model->DELETED = '0';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(Yii::$app->request->referrer);
//        return $this->redirect(['index']);
    }

    public function actionDeletePermanent($id) {
//        $model = $this->findModel($id);
//        if(Inventories::checkRelation($id))
//        $dbtransac = Yii::$app->db->beginTransaction();
//        try {
//            $dbtransac->commit();
//            Yii::$app->notify->success(' Data berjaya dihapuskan.');
//        } catch (\Exception $e) {
////                rollback operation on failure
//            $dbtransac->rollback();
//            Yii::$app->notify->fail($e->getMessage());
//        }
        if (!Inventories::deletePermanent($id)) {
            Yii::$app->notify->fail(' Data tidak berjaya dihapuskan.');
        } else {
            Yii::$app->notify->success(' Data berjaya dihapuskan. Data ini terlibat dalam transaksi.');
        }
        return $this->redirect(Yii::$app->request->referrer);
//        return $this->redirect(['index']);
    }

    public function actionSynchronizeQuantity($id) {
        Inventories::updateQuantity($id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Inventories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Inventories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Inventories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findInventory($id) {
        if (($model = Inventories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findInventoriesCheckin($id) {
        if (($model = \common\models\InventoriesCheckIn::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getInventoryQuantity($id) {
        $quantity = \common\models\InventoryItems::find()
                ->joinWith('inventory')
                ->where(['INVENTORIES.ID' => $id])
                ->andWhere(['CHECKOUT_TRANSACTION_ID' => null])
                ->andWhere(['INVENTORY_ITEMS.DELETED' => 0])
                ->asArray()
                ->count();
        return $quantity;
    }

    protected function updateInventoryQuantity($id) {
        if (($model = Inventories::findOne($id)) !== null) {
            $quantity = $this->getInventoryQuantity($id);
            $model->QUANTITY = $quantity;
            $model->save();
        }
    }

}
