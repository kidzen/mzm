<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\StockItems;
use common\models\Stocks;
use common\models\EstorInventories;
use common\models\Categories;
use common\models\Vendors;
use common\models\StocksSearch;
use common\models\ReportAllSearch;
use common\models\EstorInventoriesSearch;
use common\models\CategoriesSearch;
use common\models\VendorsSearch;
use common\models\StockItemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;

class ReportController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'deletepermanent' => ['POST'],
                    'recover' => ['POST'],
                    'delete-permanent' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
//                    [
//                        'actions' => ['index'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['Administrator'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['Pegawai Stor', 'Administrator'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
//        $model = new \common\models\ReportAll();
        $searchModel = new ReportAllSearch();
        $searchModel->YEAR = date('Y');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    public function actionPdfReport() {
//        $model = new \common\models\ReportAll();
        $searchModel = new ReportAllSearch();
        $searchModel->YEAR = date('Y');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

}
