<?php include '_head.php'; 


?>
<!--<div class="container-fluid">-->

<div id="pageTitle" value="admin"></div>
<!-- ##########  EDIT DI KAWASAN BAWAH INI SAHAJA!  ########-->



<div class="row">
 <div class="col-xs-12 main">
<div class="box">
<div class="box-body">    
          


<!--<h3 class="sub-header">Senarai Penuh Staf</h3>  -->
     

<div class="row placeholders" style="margin-top: 30px;margin-bottom: -30px;">
    
               <div class="col-xs-6 col-sm-3 placeholder">
               <a href="admin_staff"><span style="font-size: 7em;color:#ff0000" class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
               <h4>Staf</h4>
               <span class="text-muted">Senarai Penuh</span>
               </div>
    
                <div class="col-xs-6 col-sm-3 placeholder">
               <a href="admin_cuti"><span style="font-size: 7em;color: #363636" class="glyphicon glyphicon-calendar" aria-hidden="true"></span></a>
               <h4>Cuti</h4>
               <span class="text-muted">Mengikut Jabatan</span>
               </div>
    
                <div class="col-xs-6 col-sm-3 placeholder">
               <a href="admin_jabjawatan"><span style="font-size: 7em;color: #363636" class="glyphicon glyphicon-copy" aria-hidden="true"></span></a>
               <h4>Jabatan / Jawatan</h4>
               <span class="text-muted">Senarai Penuh</span>
               </div>
    
                <div class="col-xs-6 col-sm-3 placeholder">
               <a href="admin_settings"><span style="font-size: 7em;color: #363636" class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>
               <h4>Tetapan</h4>
               <span class="text-muted">Senarai Tetapan Aplikasi</span>
               </div>
    
               

            
</div>









<h3 class="sub-header"></h3> 




      <div class="panel panel-default" style="width: 100%">
                        <div class="panel-heading">
                             <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#staff">Staff</a></li>
  <li><a data-toggle="tab" href="#admin">Admin</a></li>

</ul> 
                        </div>
                        <div class="panel-body">
      
      
      
      
      
      
 
                            
                            
 <div class="tab-content">
  <div id="staff" class="tab-pane fade in active">
    <h3></h3>                           
      
    <p style="min-height: 38px"></p>    
      

    <table class="table table-bordered table-responsive table-striped  table-hover table-condensed " id="tableMain" >
        <thead class="theadx" >
    
                        
                        <th style="text-align: center;">No. Staf</th>
                        <th style="text-align: center;">Nama Penuh</th>
                        <th style="text-align: center;">Jabatan</th>
                        <th style="text-align: center;">Jawatan</th>
                       <th style="text-align: center;">Kelayakan Asal(hari)</th>
                       <th style="text-align: center;">Baki Tahun <?php echo date('Y', strtotime('-1 years')) ?></th>
                       <th style="text-align: center;">Baki Tahun <?php echo date('Y', strtotime('0 years')) ?></th>
                        <th style="text-align: center;">Total Kelayakan (hari)</th>
                        <th style="text-align: center;">Status Akaun</th>
                     <th style="text-align: center;">Tindakan</th>
                    </thead>
                    <tbody>
                        
                        
       <?php
    


        foreach ($this->semuaStaff as $row)
        {
           
          
             
           echo'<tr >
              
               <td> '.$row['staff_staffno'].'</td>
               <td> '.$row['staff_namapenuh'].'</td>
               <td >'.$row['dept_name'].'</td>
               <td >'.$row['p_name'].'</td>
               <td >'.$row['p_cuti_count'].'</td>
               <td >'.$row['bc_baki_tahun_lepas'].'</td>
               <td >'.$row['tahun_ini'].'</td>
                <td >'.$row['bc_baki'].'</td>';
                
           
                   
           switch ($row['staff_accstatus']) {
           case 'active': echo '<td ><span class="label label-success col-md-12">Aktif</span></td>';
           break;
           
           case 'blocked': echo '<td><span class="label label-warning col-md-12">Disekat</span></td>';
           break;
       
           case 'deleted': echo '<td><span class="label label-default col-md-12">Dibatalkan</span></td>';
           break;
       
           default:
           break;
           }    
                
           
           
           
            echo'<td style="max-width:120px"><center><div class="btn-group">'
           . '<button title="Kemaskini Staf."  type="button" class="btn btn-default btn-sm kemasStaff" value="'.$row['staff_id'].'" ><span class="glyphicon glyphicon-info-sign"></span></button>'
           . '<button title="Delete."  type="button" class="btn btn-default btn-sm hapusStaff" value="'.$row['staff_id'].'" ><span class="glyphicon glyphicon-remove-sign"></span></button></div></center></td></tr>';
                echo'</tr>';
        }        
      ?>                
                    </tbody>
                    
    </table>    
                            
  </div>
     
     
     
     
     
     
     
     
     
   <div id="admin" class="tab-pane fade">
    <h3></h3>                           
      
    <p style="min-height: 38px"></p>    
      

    <table class="table table-bordered table-responsive table-striped  table-hover table-condensed " id="tableAdmin" >
        <thead class="theadx" >
    
                        <th style="text-align: center;">Jabatan</th>
                        <th style="text-align: center;">Jawatan</th>
                        <th style="text-align: center;">No. Staf</th>
                        <th style="text-align: center;">Nama Penuh</th>
                       <th style="text-align: center;">Level Akses</th>
                      <th style="text-align: center;">Tindakan</th>
                    </thead>
                    <tbody>
                        
                        
       <?php
    

 
        foreach ($this->semuaAdmin as $row)
        {
           
          
             
           echo'<tr >
               <td >'.$row['dept_name'].'</td>
               <td >'.$row['p_name'].'</td>
               <td>'.$row['staff_staffno'].'</td>
               <td> '.$row['staff_namapenuh'].'</td>
               <td >'.$row['akses_level'].' : '.$row['akses_nama'].'</td>';
                
                
           
                   
          
                
           
           
           
            echo'<td style="max-width:120px"><center><div class="btn-group">'
           . '<button title="Ubah Akses"  type="button" class="btn btn-default btn-sm kemasAdmin" value="'.$row['staff_staffno'].'" ><span class="glyphicon glyphicon-info-sign"></span></button>'
           . '<button title="Delete."  type="button" class="btn btn-default btn-sm hapusAdmin" value="'.$row['staff_staffno'].'" ><span class="glyphicon glyphicon-remove-sign"></span></button></div></center></td></tr>';
                echo'</tr>';
        }        
      ?>                
                    </tbody>
                    
    </table>    
                            
  </div>  
     
     
     
     
     
     
     

     
     
     
</div>
                            
                       
      
                        

<!-- ##########  EDIT DI KAWASAN ATAS INI SAHAJA!  ########-->
</div> </div>
   </div><!-- /.box-body -->
   </div><!-- /.box --> 
</div><!-- tutup Col --> 
</div><!-- tutup Row --> 



<div id="kemaskiniAdmin" class="modal fade">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	<button data-dismiss="modal" class="close" type="button">×</button>
        <h5><b>Tukar Akses</b></h5>
	</div>
	<div class="modal-body">

            <div id="respondMod"></div> 
            
            <table class="table table-striped table-bordered table-hover table-condensed" > 
                
                <input type="hidden" id="usrid" value="" >
                
                <tr><td>Akses Terkini </td><td ><center> <div id="aksesUsr"></div></center> </td></tr>
                
                
                
            <tr><td>Akses Baru</td><td >       
                <center><select name="jenisAkses" id="jenisAkses" style="text-align: center;" required>
                
           <?php
           
           foreach ($this->semuaAkses as $row)
        {
               echo "<option  value=".$row['akses_level'].">".$row['akses_level']." : ".$row['akses_nama']."</option>";  
       
                 
               
        }
           
           ?>
                       
            </select></center>	
                    </td></tr>
</table>   

            
            
            
            
            
            
        </div>
            <div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button"  id="confirmAdmin" class="btn btn-primary">Kemaskini</button>
	</div>
       
        </div>
</div>

</div>

    
    
    
    
    
    
    
     <!--    ########################  CODE UNTUK BOOTSTRAP POPUP MULA ########################     -->      
    
    
         <div id="tukarProfilStaff" class="modal fade">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	<button data-dismiss="modal" class="close" type="button">×</button>
        <h5><b>Kemaskini Profil</b></h5>
	</div>
	<div class="modal-body">
	
<!--         ------------------------------   -->
        <div class=" table-responsive">
            <input type="hidden" id="hiddenStaffid">
                <table class="table table-condensed" >
                <tr><td colspan="2" align="left" style="padding-top: 10px;padding-bottom: 10px;"><b>PROFIL</b></td></tr>
                    <tr>
                      <td >Staf</td>
                      <td><font size="2" face="Arial" >
                          <strong>
                              <input style="text-align: center" type="text" size="4" name="stafno" id="stafnox" title="Isikan id anda" placeholder="No. Staf" >
                              : 
                              <input type="text" style="text-align: center" name="namax" id="namax" title="Isikan nama anda" placeholder="Nama penuh anda" size="40" >
                                     </strong>
                                     </font>
                                     
                                     </td>
                    </tr>
		
					
                <tr>
                      <td> Emel</td>
                      <td><font size="2" face="Arial"><strong>
                            <input type="text" name="emelx" id="emelx" size=20 data-rule-required="true" data-rule-email="true" data-msg-required="Emel diperlukan" data-msg-email="Masukkan emel yang sah!" placeholder="Emel" value="">
                          </strong></font>
                      </td>
                </tr>			
						
		<tr>
                      <td>Jabatan</td>
                      <td><font size="2" face="Arial"><strong>
                              <select name="jabatan" id="jabatanProfile" title="Pilih JABATAN anda"  >
                                  
                                  
                                  
<!--          <?php  //foreach ($this->listJab  as $row)
          { 
         ?>
             
              <option value=<?php //echo $row["dept_id"]; ?> <?php //echo $row["dept_id"]==$_SESSION['tempJab']?"selected":false ?> ><?php //echo $row["dept_name"]; ?></option>
          <?php
         }
         ?>         -->
                                  
                       
                      </select>
                          </strong></font></td>
                    </tr>
					
			<tr>
                        <td >Jawatan</td>
                        <td><font size="2" face="Arial"><strong>
                        <select name="jawatan" id="jawatanProfile" title="Pilih JAWATAN " required>
            
                     </select>
                            </strong></font></td>
                    </tr>
						
                    <tr><td colspan="2" align="left" style="padding-top: 10px;padding-bottom: 10px;"><b>KATALALUAN</b></td></tr>
		    <tr>
                      <td >Katalaluan</td>
                      <td ><font size="2" face="Arial">
                      <input type="password" NAME="pwd" size="20" id="pwdx" title="Isikan katalaluan anda" placeholder="Katalaluan BARU" >
                    </tr>
						
						
						
						
						
		      <tr>
                      <td >Ulang Katalaluan</td>
                      <td ><font size="2" face="Arial">
                      <input type="password" name="ulangpwd" id="ulangpwdx" size="20" title="Ulang katalaluan anda" placeholder="Ulang katalaluan BARU"  >
                      </tr>				
			
                      
            </table> </div>
            
        
<!--         -----------------------------   -->
	</div>
	      <div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button"  id="confirmStaff" class="btn btn-primary">Kemaskini</button>
	</div>
       
        </div>
</div>

</div>
         
  <!--    ########################  CODE UNTUK BOOTSTRAP POPUP TAMAT  ########################     -->        
    
    
    
    
    
    
    
    
    
    
    
    

<script type="text/javascript">
    $(document).ready(function ()
  { 
     
     
     
      $('#jabatanProfile').change(function(){
    
    var deptid = $(this).val();
   
    $("#jawatanProfile").empty();
    
  
     $.ajax({
                           url :'call/jawatanListByDept',
                           type: 'POST',
                           data: 'deptid='+deptid,
                           dataType: 'JSON',
                           cache: false,
                           
                            success: function(data,status,xhr)
                            {
                                 
                               $.each(data.result,function()
                               {
                                   
                                   $("#jawatanProfile").append("<option value=\""+this["p_id"]+"\" >"+this["p_name"]+"</option>");
                               });
                              
                           }
                     });
  
    });
    
    
    
    
    
    
    
    
    
    
        $('.kemasStaff').click(function()
        { 
            
            
            var stafid = $(this).val();
            
          $('#tukarProfilStaff').modal(
            {
                backdrop: 'static',
                keyboard: true
              
              
            });
            
            
            
            
            
                      $.ajax({
                       url: 'call/callStaffById',
                       data: {stafid:stafid},
                       dataType: 'json',
                       cache: false,
                       type: 'POST',
                       success: function(data){
                       //alert(<?php //echo $_SESSION['tempJab'];?>);
                       //$(location).attr('href','main');
                               $.each(data ,   function(k,v)
                               {
                                                   $('#hiddenStaffid').val(stafid);
                                                   $('#stafnox').val(v.staffno);
                                                   $('#namax').val(v.nama);
                                                   $('#emelx').val(v.emel);
                                                
                                                
                                                
                                                $("#jawatanProfile").empty();
                          
  
                                            $.ajax({
                                               url :'call/jawatanListByDept',
                                               type: 'POST',
                                               data: 'deptid='+v.jabatan,
                                               dataType: 'JSON',
                                               cache: false,

                                                success: function(data,status,xhr)
                                                {

                                                   $.each(data.result,function()
                                                   {
                                                       if(v.jawatan==this["p_id"])
                                                       {
                                                        $("#jawatanProfile").append("<option value=\""+this["p_id"]+"\" selected >"+this["p_name"]+"</option>");  
                                                       }
                                                       else
                                                       {
                                                        $("#jawatanProfile").append("<option value=\""+this["p_id"]+"\"  >"+this["p_name"]+"</option>");     
                                                       }
                                                       
                                                  
                                                   });

                                                   }
                                                });
                                                
                                                
                                                
                                                
                                                $("#jabatanProfile").empty();
                                                
                                                
                                                
                                                $.ajax({
                                               url :'call/callAllDeptx',
                                               data:'deptid='+v.jabatan,
                                               type: 'POST',
                                               dataType: 'JSON',
                                               cache: false,

                                                success: function(data,status,xhr)
                                                {

                                                   $.each(data,function()
                                                   {
                                                       
                                                       if(v.jabatan==this["deptid"])
                                                       {
                                                        $("#jabatanProfile").append("<option value=\""+this["deptid"]+"\" selected >"+this["deptname"]+"</option>");  
                                                       }
                                                       else
                                                       {
                                                        $("#jabatanProfile").append("<option value=\""+this["deptid"]+"\"  >"+this["deptname"]+"</option>");     
                                                       }
                                                       
                                                  
                                                   });

                                                }
                                              });
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                              });
                               
                               
                               
                                           
   
                                            
                     
                     
                     
                     
                     
                  
                       },
                        error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus+" "+errorThrown);
                      }
                   });
            
            

            
            
        });
    
    
    
    //simpan data selepas update
       $('#confirmStaff').click(function()
        { 
            var idx = $('#hiddenStaffid').val();
            var nox = $('#stafnox').val();
            var namaBaru = $('#namax').val();
            var emelBaru = $('#emelx').val();
            
            
            var depBaru= $('#jabatanProfile').val();
            var pBaru= $('#jawatanProfile').val(); 
            
            var pwdBaru = $('#pwdx').val(); 
            var upwdBaru = $('#ulangpwdx').val(); 
            
        
            
            
        if (pwdBaru!=upwdBaru)
        {
          alert('Katalaluan tidak sepadan!')  ;
        }
        
        
          else
          {
       
             $.ajax({
                       
                       url: 'call/updateProfilestaff',
                       data: {id:idx,nostaff:nox,nama:namaBaru,emel:emelBaru,jab:depBaru,jaw:pBaru,pass:pwdBaru},
                       cache: false,
                       type: 'POST',
                       success: function(data)
                       {
                           
                          if(data>0)
                          {
                             // alert('Data berjaya dikemaskini!');
                              $(location).attr('href','admin_staff');
                          }
                          else
                          {
                            alert('Gagal! Mohon cuba sekali lagi.');  
                          }
                     
                       
                  
                       },
                        error: function(jqXHR, textStatus, errorThrown) {
                            
                            
                        console.log(textStatus+" "+errorThrown);
                      }
                   });
                
                
              }  
        
       
                
                
                
                
                
                
                
                
                
             
            }); 
    
    
    
     
     
     
             //delete permohonan
       $('.hapusStaff').click(function()
        {
             //e.preventDefault();
                //alert('hehe');
                var currval=$(this).attr('value');  
                swal({   title: "Anda pasti?", 
                         text:"Padam staff dari sistem." , 
                         type: "warning",   
                         showCancelButton: true,   
                         confirmButtonColor: "#DD6B55",   
                         confirmButtonText: "Ya",
                         cancelButtonText: "Tidak",
                         closeOnConfirm: true }, 
                     function(){ 
                         
                       
                   $.ajax({
                       url: 'call/deleteStaff',
                       data: {id:currval},
                       cache: false,
                       type: 'POST',
                       success: function(data){
                       //alert(data);
                       $(location).attr('href','admin_staff');
                  
                       },
                        error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus+" "+errorThrown);
                      }
                   });
                               
             
            });
            
            
            
            
        });
     
     
     
     
     
     
     
     
     
     
     
    $('.kemasAdmin').click(function()
     { 
     var tempval = $(this).attr('value');
     
          $('#kemaskiniAdmin').modal(
            {
                backdrop: 'static',
                keyboard: true
              
              
            });
            
            
                       $.ajax({
                       url: 'call/callAkses',
                       data: {id:tempval},
                       dataType: 'json',
                       cache: false,
                       type: 'POST',
                       success: function(data){
                       //alert(data);
                       //$(location).attr('href','main');
                               $.each(data ,   function(k,v)
                                               {
                                                   
                                                   $('#usrid').val(tempval);
                                                    $('#aksesUsr').html(v.aksesNo + ' : ' + v.aksesName);
                                                    //alert(v.aksesNo + ' : ' + v.aksesName);
                                                    
                                               
                                               }
                                       );
                  
                       },
                        error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus+" "+errorThrown);
                      }
                   });
     
     });
        
        //delete admin
       $('.hapusAdmin').click(function()
        {
             //e.preventDefault();
                //alert('hehe');
                var currval=$(this).attr('value');  
                swal({   title: "Anda pasti?", 
                         text:"Padam staf dari senarai admin." , 
                         type: "warning",   
                         showCancelButton: true,   
                         confirmButtonColor: "#DD6B55",   
                         confirmButtonText: "Ya",
                         cancelButtonText: "Tidak",
                         closeOnConfirm: true }, 
                     function(){ 
                         
                       
                   $.ajax({
                       url: 'call/deleteAdmin',
                       data: {id:currval},
                       cache: false,
                       type: 'POST',
                       success: function(data){
                       alert("Berjaya!");
                       //$(location).attr('href','utama');
                  
                       },
                        error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus+" "+errorThrown);
                      }
                   });
                               
             
            });
            
            
            
            
        });

        
        
      
        
        
        
       //      //update
//      
        $('#confirmAdmin').click(function()
        { 
//            
//            
            var idx = $('#usrid').val();
            var jenisAkses = $('#jenisAkses').val();
          

//            
//             //e.preventDefault();
//    
                   $.ajax({
                       
                       url: 'call/updateAdmin',
                       data: {id:idx,akses:jenisAkses},
                       cache: false,
                       type: 'POST',
                       success: function(data){
                           
                           $('#respondMod').append('<div style=\"min-height: 50px\" class=\"alert alert-success\">Kemaskini berjaya!</div>');
                                        setTimeout(function() {
                                        $('#respondMod').fadeTo(300, 0).slideUp(300, function(){
                                            $(this).remove(); 
                                        });
                                        $('#kemaskiniAdmin').modal('hide');
                                    }, 1100);
                            
                       
                       
                  
                       },
                        error: function(jqXHR, textStatus, errorThrown) {
                            
                            $('#respondMod').append('<div style=\"min-height: 50px\" class=\"alert alert-danger\">Ralat.Cuba sekali lagi</div>');
                                        setTimeout(function() {
                                        $('#respondMod').fadeTo(300, 0).slideUp(300, function(){
                                            $(this).remove(); 
                                        });
                                    }, 2000);
                        console.log(textStatus+" "+errorThrown);
                      }
                   });
                               
             
            }); 
        
        
        
        
        
        
        
        
            
  });          
</script>
   



<?php include '_foot.php'; ?>
