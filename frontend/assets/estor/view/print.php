<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="mmh2050">
<!--    <link rel="icon" href="../../favicon.ico">-->

    <title>eCuti Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo BASEDIR; ?>engine/view/css/signin.css" rel="stylesheet">
    <style type="text/css">
<!--
p.MsoNoSpacing {
margin:0cm;
margin-bottom:.0001pt;
font-size:11.0pt;
font-family:"Calibri","sans-serif";
}
p.MsoNormal {
margin-top:0cm;
margin-right:0cm;
margin-bottom:10.0pt;
margin-left:0cm;
line-height:115%;
font-size:11.0pt;
font-family:"Calibri","sans-serif";
}
table.MsoNormalTable {
font-size:10.0pt;
font-family:"Calibri","sans-serif";
}
-->
</style>

    <script src="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo BASEDIR; ?>engine/view/js/jquery-1.11.3.min.js" type="text/javascript"></script>
   
        
        

   
  </head>
  <body onload="window.print()">
      <div class="container">
          <div class="row" style="margin-left:10px;">
<table><tr><td>
 
            
 
            
            
            
            
            
            
            
            
            
  <p class="MsoNoSpacing" align="center" style="text-align:center;">&nbsp; </p>
<div>
    <p class="MsoNormal" style="float: right;"><span style="line-height:115%; font-size:8.0pt; ">PICOMS/SM/Cuti-06/09</span><span style="line-height:115%; font-size:6.0pt; ">(Pindaan2/10)</span><span style="font-size:8.0pt; "> </span></p>
</div>
<span style="font-family:'Arial','sans-serif'; ">&nbsp;</span>
</p>
<br clear="all" />

<p class="MsoNoSpacing" align="center" style="text-align:center;"><img style="margin-top: -40px;margin-left: 120px;width: 400px;" src ="<?php echo BASEDIR; ?>engine/view/img/header.png" class="img-responsive" draggable="false"> <strong></strong></p>

<p class="MsoNoSpacing" align="center" style="text-align:center;"><strong><span style="font-size:16.0pt; ">BORANG  PERMOHONAN CUTI </span></strong></p>
<p class="MsoNoSpacing">&nbsp; </p>
<div>
 
</div>
<?php foreach ($this->detStaff as $row)
        {
    

?>
<strong><span style="font-size:12.0pt; "><center><?php echo $row['cuti_jeniscuti']; ?></center></span></strong>
</p>

<p class="MsoNoSpacing"><strong><span style="font-size:12.0pt; ">&nbsp;</span></strong></p>
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="621" style="width:466.1pt;border-collapse:collapse;border:none;">
  <tr>
    <td width="621" valign="top" style="width:466.1pt;border:solid black 1.0pt;background:#1F497D;padding:0cm 5.4pt 0cm 5.4pt;" bgcolor="#1F497D"><p class="MsoNormal" style="margin-bottom:.0001pt;line-height:normal;">&nbsp;</p></td>
  </tr>
</table>
<p class="MsoNoSpacing">  </p>

<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">NAMA : <strong><?php echo $row['staff_namapenuh']; ?></strong>                                       
       NO. STAF: <strong><?php echo $row['staff_staffno']; ?></strong></span></p>




<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">JAWATAN  :<strong> <?php echo $row['p_name']; ?></strong>                                          BAHAGIAN  :  <strong><?php echo $row['dept_name']; ?></strong></span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">            .</span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; text-transform:capitalize;">Tarikh  cuti dipohon : <strong><?php echo $row['tarikhmula']; ?></strong>                sehingga: <strong><?php echo $row['tarikhtamat']; ?>    </strong>               Jumlah baki cuti: <strong><?php echo $row['baki_cuti']; ?> </strong> hari</span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt;text-transform:capitalize;  ">Sebab  cuti dipohon : <strong><?php echo $row['cuti_sebabcuti']; ?></strong></span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">Pengganti  : <strong><?php echo $row['ganti_namapenuh']; ?></strong></span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">Tarikh Pohon  : <strong><?php echo $row['tarikhapply']; ?></strong></span></p>
<p class="MsoNoSpacing"><strong><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></strong></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">NAMA  KETUA UNIT/ BAHAGIAN             : <strong><?php echo $row['bos_namapenuh']; ?></strong></span></p>
<p class="MsoNoSpacing"><strong><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></strong></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">JABATAN                                                : <strong><?php echo $row['jabatan_bos']; ?></strong></span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; text-transform: uppercase; ">STATUS DARI  KETUA UNIT/ BAHAGIAN   : <strong><?php echo $row['cuti_status'] ?></strong></span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; text-transform: uppercase;">TARIKH  <?php echo $row['cuti_status'] ?>                                                : <strong><?php echo $row['tarikhapp']; ?> </strong></span></p>
<p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></p>

<p class="MsoNoSpacing" align="center" style="text-align:center;"><strong><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">_________________________________________________________________________________</span></strong></p>
<p class="MsoNoSpacing" align="center" style="text-align:center;"><strong><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">UNTUK KEGUNAAN PEJABAT</span></strong></p>
<p class="MsoNoSpacing" align="center" style="text-align:center;"><strong><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></strong></p>
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none;">
  <tr>
    <td width="300" valign="top" style="width:225.15pt;border:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;"><p class="MsoNoSpacing"><strong><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">REKOD CUTI    TAHUNAN  DAN LAIN-LAIN</span></strong></p></td>
    <td width="316" valign="top" style="width:236.95pt;border:solid black 1.0pt;border-left:none;padding:0cm 5.4pt 0cm 5.4pt;"><p class="MsoNoSpacing" align="center" style="text-align:center;"><strong><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">REKOD CUTI TANPA GAJI</span></strong></p></td>
  </tr>
  <tr>
    <td width="300" valign="top" style="width:225.15pt;border:solid black 1.0pt;border-top:none;padding:0cm 5.4pt 0cm 5.4pt;"><p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">&nbsp;</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">Kelayakan    Cuti tahun ini           : <strong><?php echo $row['tahun_ini'] ?> </strong> hari</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">&nbsp;</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">Baki    Cuti Yang di bawa ke hadapan  : <strong><?php echo $row['bc_baki_tahun_lepas'] ?> </strong>hari</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">&nbsp;</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">Jumlah    cuti yang telah diambil </span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">(termasuk    cuti yang dipohon)    :  <strong><?php echo $row['bc_lulus_tahun_ini'] ?></strong>   hari</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">&nbsp;</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">Baki    kelayakan cuti terkini        :  <strong><?php echo $row['bc_baki_tahun_ini'] ?></strong> hari</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">&nbsp;</span></p>
      <p class="MsoNoSpacing" align="center" style="text-align:center;"><strong><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></strong></p></td>
    <td width="316" valign="top" style="width:236.95pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;"><p class="MsoNoSpacing" align="center" style="text-align:center;"><strong><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></strong></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">Potongan    gaji bulan : …………...................</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">&nbsp;</span></p>
      <p class="MsoNoSpacing"><u><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">Formula    : Gaji bulanan/Elaun   x  bilangan hari bercuti</span></u><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">     </span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">                            bilangan hari  bulan berkenaan</span></p>
      <p class="MsoNoSpacing"><span style="font-family:'Arial','sans-serif'; font-size:9.0pt; ">&nbsp;</span></p>
      <p class="MsoNoSpacing"><strong><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></strong></p></td>
  

<?php

        }
?>

</tr>
</table>
<p class="MsoNoSpacing" align="center" style="text-align:center;"><strong><span style="font-family:'Arial','sans-serif'; font-size:10.0pt; ">&nbsp;</span></strong></p>
<p class="MsoNoSpacing"><strong><span style="font-size:12.0pt; ">Bagi Baki Cuti Yang di bawa ke Tahun hadapan <u>PERLU </u>dihabiskan sebelum <u>1 MAY</u>, setiap tahun.</span></strong></p>          
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
</td></tr></table>   
      
</div>   
    </div> <!-- /container -->


    
  </body>
</html>



