<?php
namespace frontend\models;

use common\models\People;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $EMAIL;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
//            ['EMAIL', 'email'],
            ['EMAIL', 'exist',
                'targetClass' => '\common\models\People',
                'filter' => ['DELETED' => People::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = People::findOne([
            'DELETED' => People::STATUS_ACTIVE,
            'EMAIL' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        
        if (!People::isPasswordResetTokenValid($user->PASSWORD_RESET_TOKEN)) {
            $user->generatePasswordResetToken();
        }
        
        if (!$user->save()) {
            return false;
        }
//        return true;
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
//            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for e-Store'  )//. \Yii::$app->name)
            ->send();
    }
}
