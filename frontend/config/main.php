<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;

$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
//    'bootstrap' => ['log'],
    'bootstrap' => ['log', 'debug'],
//    'bootstrap' => ['log','gii','debug'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'class' => 'common\components\User',
            'identityClass' => 'common\models\People',
            'enableAutoLogin' => true,
        ],
        'notify' => [
            'class' => 'common\components\Notify',
        ],
//        'request' => [
// //            'csrfParam' => '_csrf-frontend',
//            // 'baseUrl' => '/estor',
//             'parsers' => [
//                 'application/json' => 'yii\web\JsonParser',
//             ]
//         ],
        'assetManager' => [
            'linkAssets' => true,
//             'forceCopy' => true,
            'appendTimestamp' => true,
        ],
        'access' => [
            'class' => 'common\components\AccessRule',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
//         'urlManager' => [
//             'class' => 'yii\web\UrlManager',
// //            'enablePrettyUrl' => true,
// ////            'baseUrl' => '/estor',
// ////            'showScriptName' => false,
// ////            'enableStrictParsing' => false,
// ////            'rules' => [
// ////                'home' => 'frontend/web',
// ////////                'home' => 'site/index', 
// ////////                'laporan' => 'laporan/index', 
// ////////                'pengesahan' => 'pengesahan/index', 
// //////                '<alias:\w+>/<controller:\w+>/<action:\w+>' => 'site/<alias>',
// ////            ],
//         ],
       // 'urlManager' => [
       //      'enablePrettyUrl' => true,
       //      'enableStrictParsing' => true,
       //      'showScriptName' => false,
       //      'rules' => [
       //          ['class' => 'yii\rest\UrlRule', 'controller' => 'user'],
       //      ],
       //  ],
    ],
    'modules' => [
        // If you use tree table
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        // see settings on http://demos.krajee.com/tree-manager#module
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
        ],
        'dynagrid' => [
            'class' => '\kartik\dynagrid\Module',
        ],
        'debug' => ['class' => 'yii\debug\Module'], //remove on production
        'gii' => [
            'class' => 'yii\gii\Module',
//            'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
            'generators' => [//here
                'crud' => [// generator name
                    'class' => 'yii\gii\generators\crud\Generator', // generator class
                    'templates' => [//setting for out templates
                        'myCrud' => '@frontend/components/templates/crud/default', // template name => path to template
                    ]
                ]
            ],
        ],
//        'debug' => [
//            'class' => 'yii\debug\Module',
//        ],
    ],
//    $config['bootstrap'][] = 'debug';
//    $config['bootstrap'][] = 'gii';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//    ];
//    $config['modules']['gii'] = [
//        'class' => 'yii\gii\Module',
//    ];
    'params' => $params,
];
