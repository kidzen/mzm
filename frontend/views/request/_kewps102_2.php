
<tbody>
    <?php foreach ($items as $item) { ?>
        <tr>
            <td class="col-1 row-1" style="width:10%"><?= $item['inventory']['CODE_NO'] ?></td>
            <td class="col-2 row-1" style="width:40%" colspan="2"><?= mb_strimwidth($item['inventory']['DESCRIPTION'],0,35,"...",'UTF-8') ?></td>
            <td class="col-3 row-1" style="width:7%"><?= $item['RQ_QUANTITY'] ?></td>
            <td class="col-4 row-1" style="width:7%"><?= $item['inventory']['CARD_NO'] ?></td>
            <td class="col-5 row-1" style="width:7%"><?= $item['CURRENT_BALANCE'] ?></td>
            <td class="col-6 row-1" style="width:7%"><?= $item['APP_QUANTITY'] ?></td>
            <td class="col-7 row-1" style="width:7%"><?= $item['UNIT_PRICE'] / $item['APP_QUANTITY'] ?></td>
            <td class="col-8 row-1" style="width:7%;text-align: center;"><?= $item['UNIT_PRICE'] ?></td>
            <td class="col-9 row-1" style="width:8%"><?= $item['APP_QUANTITY'] ?></td>
            <td class="col-10 row-1" style="width:15%"></td>
        </tr>
    <?php } ?>
<tbody>