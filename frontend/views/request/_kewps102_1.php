

<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN A</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="pull-right form-id"><strong>KEW.PS-10</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>BORANG PESANAN DAN PENGELUARAN STOK</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 row-1" colspan="4"><span class="row-1-label">Daripada :</span><br><span class="row-1-data"> <br><br></span></th>
                    <th class="col-2 row-1" colspan="7"><span class="row-1-label">Kepada :</span><br><span class="row-1-data"> <br><br></span></th>
                </tr>
                <tr>
                    <th class="text-center info col-1 row-2" colspan="4">Dilengkapkan Oleh Stor Pesanan</th>
                    <th class=" right-border-bold col-2 row-2" class="text-center info" colspan="7">Dilengkapkan Oleh Stor Pengeluar</th>
                </tr>
                <tr>
                    <th class="col-1 row-3" colspan="4">No Pemesanan : <?= mb_strimwidth($items[0]['order']['ORDER_NO'],0,20,"...",'UTF-8') ?></th>
                    <th class="col-2 row-3 right-border-bold" colspan="7">No Pengeluaran : <?= mb_strimwidth($items[0]['order']['ORDER_NO'],0,20,"...",'UTF-8') ?></th>
                </tr>
                <tr>
                    <th class="col-1 row-4" colspan="4">Tarikh Bekalan Dikehendaki : </th>
                    <th class="col-2 row-4 text-center info right-border-bold" colspan="5">BAHAGIAN BEKALAN,KAWALAN DAN AKAUN</th>
                    <th class="col-3 row-4 text-center info" colspan="2">BAHAGIAN SIMPANAN</th>
                </tr>
                <tr>
                    <th class="col-1 row-5 text-center info" rowspan="2">No. Kod</th>
                    <th class="col-2 row-5 text-center info" colspan="2" rowspan="2">Perihal Stok</th>
                    <th class="col-3 row-5 text-center info" rowspan="2">Kuantiti</th>
                    <th class="col-4 row-5 text-center info" colspan="2">Kad Kawalan Stok</th>
                    <th class="col-5 row-5 text-center info" rowspan="2">Kuantiti Diluluskan</th>
                    <th class="col-6 row-5 text-center info" colspan="2">Harga (RM)</th>
                    <th class="col-7 row-5 text-center info" rowspan="2">Kuantiti Dikeluarkan</th>
                    <th class="col-8 row-5 text-center info" rowspan="2">Catatan</th>
                </tr>
                <tr>
                    <th class="col-1 row-6 text-center info">No. Kad</th>
                    <th class="col-2 row-6 text-center info">Baki Sedia Ada</th>
                    <th class="col-3 row-6 text-center info">Seunit</th>
                    <th class="col-4 row-6 text-center info">Jumlah</th>
                </tr>
            </thead>
