<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OrderItems */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Order Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info order-items-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>order-items : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'ID',
            'INVENTORY_ID',
            'ORDER_ID',
            'RQ_QUANTITY',
            'APP_QUANTITY',
            'CURRENT_BALANCE',
            'UNIT_PRICE',
            'CREATED_AT',
            'UPDATED_AT',
            'CREATED_BY',
            'UPDATED_BY',
            [
            'attribute'=>'DELETED',
            'vAlign' => 'middle',
            'value' => $model->DELETED == 1 ? 'Deleted' : 'Active',
            ],
            'DELETED_AT',
    ],
    ]) ?>

</div>
