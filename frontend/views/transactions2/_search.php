<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TransactionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transactions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'TYPE') ?>

    <?= $form->field($model, 'INVENTORY_ID') ?>

    <?= $form->field($model, 'VENDOR_ID') ?>

    <?= $form->field($model, 'ITEMS_QUANTITY') ?>

    <?php // echo $form->field($model, 'ITEMS_TOTAL_PRICE') ?>

    <?php // echo $form->field($model, 'CHECK_DATE') ?>

    <?php // echo $form->field($model, 'CHECK_BY') ?>

    <?php // echo $form->field($model, 'APPROVED_BY') ?>

    <?php // echo $form->field($model, 'APPROVED_AT') ?>

    <?php // echo $form->field($model, 'CREATED_AT') ?>

    <?php // echo $form->field($model, 'UPDATED_AT') ?>

    <?php // echo $form->field($model, 'CREATED_BY') ?>

    <?php // echo $form->field($model, 'UPDATED_BY') ?>

    <?php // echo $form->field($model, 'DELETED') ?>

    <?php // echo $form->field($model, 'DELETED_AT') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
