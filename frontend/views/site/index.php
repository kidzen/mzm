<?php
/*+ PUSH_PRED(C) PUSH_PRED(D) PUSH_PRED(E) */
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\widgets\Growl;
?>
<div class="site-index">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <!--        <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>-->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua-gradient">
                    <div class="inner">
                        <h3><?= $quantityStockIn ?></h3>

                        <p>Kemasukan Stok Baru Bulan Ini</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green-gradient">
                    <div class="inner">
                        <h3><?= $quantityStockOut ?><!--<sup style="font-size: 20px">%</sup>--></h3>
                        <p>Pengeluaran Stok Bulan Ini</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow-gradient">
                    <div class="inner">
                        <h3><?= $quantityInventoriesCardRegistered ?></h3>

                        <p>Pendaftaran Kad Inventori Bulan Ini</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red-gradient">
                    <div class="inner">
                        <h3><?= $quantityStockRequest ?></h3>
                        <p>Pengesahan Pengeluaran Stok Bulan Ini</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>

        <!-- /.row -->



        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">


                <!-- /.row -->

                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Permohonan Terbaru</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <?php if (isset($latestOrder)) { ?>
                                <?=
                                yii\grid\GridView::widget([
                                    'dataProvider' => $latestOrder,
                                    'tableOptions' => ['class' => 'table no-margin'],
                                    'layout' => '{items}',
//                                'summary'=>'',
//                                'summary' => "{begin} - {end} {count} {totalCount} {page} {pageCount}",
                                    'columns' => [
//                                        ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'label' => 'No Order',
                                            'attribute' => 'order.ORDER_NO',
                                        ],
                                        [
                                            'label' => 'Kegunaan',
                                            'attribute' => 'vehicle.REG_NO',
                                        ],
                                        [
                                            'label' => 'Tarikh Permohonan',
                                            'attribute' => 'order.REQUIRED_DATE',
                                        ],
                                        [
                                            'label' => 'Pemohon',
                                            'attribute' => 'order.ORDERED_BY',
                                        ],
                                        [
                                            'label' => 'Status',
                                            'attribute' => 'order.APPROVED',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                if ($model->order->APPROVED == '1') {
                                                    return Html::tag('span', 'Approved', ['class' => 'label label-success']);
                                                } else if ($model->order->APPROVED == '2') {
                                                    return Html::tag('span', 'Pending', ['class' => 'label label-warning']);
                                                } else if ($model->order->APPROVED == '8') {
                                                    return Html::tag('span', 'Rejected', ['class' => 'label label-danger']);
                                                } else {
                                                    return Html::tag('span', 'Undefined', ['class' => 'label label-default']);
                                                }
                                            }
                                                ],
                                                [
                                                    'label' => 'Tarikh Pengesahan',
                                                    'attribute' => 'order.APPROVED_AT',
                                                ],
                                            ]
                                        ]);
                                        ?>

                                    <?php } ?>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <?= Html::a('Place New Order', ['request/create'], ['class' => 'btn btn-sm btn-info btn-flat pull-left']) ?>
                                <?= Html::a('View All Orders', ['request/index'], ['class' => 'btn btn-sm btn-default btn-flat pull-right']) ?>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->

                    <div class="col-md-4">

                        <!-- PRODUCT LIST -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Kemasukan Stok Baru</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <ul class="products-list product-list-in-box">
                                    <!-- /.item -->

                                    <?php if (isset($latestStockIn)) { ?>
                                        <?php foreach ($latestStockIn->models as $StocksInItems) { ?>
                                            <li class="item">
                                                <div class="product-info">
                                                    <?= Html::a($StocksInItems->category->NAME . Html::tag('span', 'RM ' . $StocksInItems->itemsDetails->UNIT_PRICE, ['class' => 'label label-info pull-right']), ['inventories/view','id'=>$StocksInItems->inventory->ID], ['class' => 'product-title']) ?>
                                                    <span class="product-description">
                                                        <?= $StocksInItems->inventory->DESCRIPTION ?>
                                                    </span>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>

                                    <!-- /.item -->
                                </ul>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-center">
                                <?= Html::a('View All Products', ['inventories/index']) ?>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

    <!-- Main content -->


    <!-- Content Header (Page header) -->



    <div class="clearfix"></div>


    <!--    <div class="jumbotron">
            <br>
            <h1><b>MPSP </b> e-Store </h1>
            <h1 style="color: #cc99ff;
                -webkit-text-fill-color: #cc99ff; /* Will override color (regardless of order) */
                -webkit-text-stroke-width: 2px;
                -webkit-text-stroke-color: #000099;">
                <strong>MPSP e-Store</strong>
                <strong>SISTEM PENGURUSAN KONTRAKTORISASI AGSE / AGSV</strong>
            </h1>

            <h1 style="color: black;
                letter-spacing: -3px;
                -webkit-text-fill-color: white; /* Will override color (regardless of order) */
                -webkit-text-stroke-width: 2px;
                -webkit-text-stroke-color: blue;">
                <strong>Pangkalan Udara Gong Kedak</strong></h1>
                <strong>Muzzam Teknologi Sdn Bhd</strong></h1>
            <p class="lead">Muzzam Teknologi Sdn Bhd</p>
            <p><a class="btn btn-lg btn-success" href="site/index">Pengenalan Sistem</a></p>
        </div>-->



    <!--    <div class="body-content">

            <div class="row">
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
                </div>
            </div>

        </div>-->
</div>
