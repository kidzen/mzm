<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LaporanPenyelenggaraanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="laporan-penyelengaraan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'TAHUN') ?>

    <?= $form->field($model, 'BULAN') ?>

    <?= $form->field($model, 'NO_KERJA') ?>

    <?= $form->field($model, 'ARAHAN_KERJA') ?>

    <?php // echo $form->field($model, 'JK_JG') ?>

    <?php // echo $form->field($model, 'BENGKEL_PANEL_ID') ?>

    <?php // echo $form->field($model, 'NAMA_PANEL') ?>

    <?php // echo $form->field($model, 'KATEGORI_PANEL') ?>

    <?php // echo $form->field($model, 'SELENGGARA_ID') ?>

    <?php // echo $form->field($model, 'TEMPOH_SELENGGARA') ?>

    <?php // echo $form->field($model, 'ALASAN_ID') ?>

    <?php // echo $form->field($model, 'ALASAN') ?>

    <?php // echo $form->field($model, 'TARIKH_ARAHAN') ?>

    <?php // echo $form->field($model, 'NO_SEBUTHARGA') ?>

    <?php // echo $form->field($model, 'TARIKH_KENDERAAN_TIBA') ?>

    <?php // echo $form->field($model, 'TARIKH_JANGKA_SIAP') ?>

    <?php // echo $form->field($model, 'TARIKH_SEBUTHARGA') ?>

    <?php // echo $form->field($model, 'STATUS_SEBUTHARGA') ?>

    <?php // echo $form->field($model, 'NO_DO') ?>

    <?php // echo $form->field($model, 'TARIKH_SIAP') ?>

    <?php // echo $form->field($model, 'TEMPOH_JAMINAN') ?>

    <?php // echo $form->field($model, 'TARIKH_DO') ?>

    <?php // echo $form->field($model, 'TARIKH_CETAK') ?>

    <?php // echo $form->field($model, 'STATUS') ?>

    <?php // echo $form->field($model, 'CATATAN_SEBUTHARGA') ?>

    <?php // echo $form->field($model, 'CATATAN') ?>

    <?php // echo $form->field($model, 'NAMA_PIC') ?>

    <?php // echo $form->field($model, 'TARIKH_PERMOHONAN') ?>

    <?php // echo $form->field($model, 'ID_KENDERAAN') ?>

    <?php // echo $form->field($model, 'KATEGORI_KEROSAKAN_ID') ?>

    <?php // echo $form->field($model, 'KATEGORI_KEROSAKAN') ?>

    <?php // echo $form->field($model, 'KATEGORI') ?>

    <?php // echo $form->field($model, 'ISSERVICE') ?>

    <?php // echo $form->field($model, 'ISPANCIT') ?>

    <?php // echo $form->field($model, 'ODOMETER_TERKINI') ?>

    <?php // echo $form->field($model, 'NO_PLAT') ?>

    <?php // echo $form->field($model, 'MODEL') ?>

    <?php // echo $form->field($model, 'KOD_JABATAN') ?>

    <?php // echo $form->field($model, 'NAMA_JABATAN') ?>

    <?php // echo $form->field($model, 'JUMLAH_KOS') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
