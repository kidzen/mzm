<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ApprovalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="approval-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'ID_INVENTORY_ITEMS') ?>

    <?= $form->field($model, 'ID_INVENTORIES') ?>

    <?= $form->field($model, 'ID_CATEGORIES') ?>

    <?= $form->field($model, 'ID_INVENTORIES_CHECKIN') ?>

    <?php // echo $form->field($model, 'ID_ORDER_ITEMS') ?>

    <?php // echo $form->field($model, 'ID_ORDERS') ?>

    <?php // echo $form->field($model, 'SKU') ?>

    <?php // echo $form->field($model, 'ODER') ?>

    <?php // echo $form->field($model, 'ORDER_NO') ?>

    <?php // echo $form->field($model, 'ITEMS_CATEGORY_NAME') ?>

    <?php // echo $form->field($model, 'ITEMS_INVENTORY_NAME') ?>

    <?php // echo $form->field($model, 'RQ_QUANTITY') ?>

    <?php // echo $form->field($model, 'APP_QUANTITY') ?>

    <?php // echo $form->field($model, 'APPROVED') ?>

    <?php // echo $form->field($model, 'CURRENT_BALANCE') ?>

    <?php // echo $form->field($model, 'TOTAL_PRICE') ?>

    <?php // echo $form->field($model, 'ORDER_DATE') ?>

    <?php // echo $form->field($model, 'REQUIRED_DATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
