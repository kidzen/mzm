


<div class="box-body table-responsive pdf">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN A</strong><br><strong>KEW.PS-14</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>LAPORAN PEMERIKSAAN/ VERIFIKASI STOR TAHUN <?= $year ?></strong></p>
        <p><strong>Kementerian/ Jabatan:</strong></p>
        <!-- <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap"> -->
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="row-4 text-center info border-right-bold" colspan="3">DIISI OLEH PEGAWAI STOR</th>
                    <th class="row-4 text-center info border-right-bold" colspan="4">DIISI OLEH PEGAWAI PEMERIKSA</th>
                    <th class="row-4 text-center info" colspan="7">DIISI OLEH PEMVERIFIKASI</th>
                </tr>
                <tr>
                    <th class="text-center info" rowspan="2">No. Kad Kawalan Stok</th>
                    <th class="text-center info" rowspan="2" style="width:200px">Perihal Stok</th>
                    <th class="text-center info border-right-bold" rowspan="2">Kos Seunit (RM)</th>
                    <th class="text-center info border-right-bold" colspan="4">Kuantiti Stok</th>
                    <th class="text-center info" colspan="4">Kuantiti Stok</th>
                    <th class="text-center info" colspan="2">Penemuan Keadaan Stok (usang/rosak/tidakdigunakan lagi/tidak aktif/luput tempoh/ dll)</th>
                    <th class="text-center info" rowspan="2">Syor/Cadangan (lupus/hapus kira/pelarasan stok/pindahan stok)</th>

                </tr>
                <tr>
                    <th class="text-center info">Fizikal Stok</th>
                    <th class="text-center info">Baki Di Kad Kawalan Stok</th>
                    <th class="text-center info">lebih</th>
                    <th class="text-center info border-right-bold">Kurang</th>
                    <th class="text-center info">Fizikal Stok</th>
                    <th class="text-center info">Baki Di Kad Kawalan Stok</th>
                    <th class="text-center info">lebih</th>
                    <th class="text-center info">Kurang</th>
                    <th class="text-center info">Kuantiti</th>
                    <th class="text-center info">Justifikasi</th>
                </tr>
            </thead>
