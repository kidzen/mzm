
        </table>
        <br>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
            <thead>
                <tr>
                    <th class="" colspan="12">Dilengkapkan oleh Pegawai Pemeriksa</th>
                    <th class="" colspan="12">Dilengkapkan oleh Pegawai Pemverifikasi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="pull-left" colspan="2">Tandatangan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Tandatangan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Tandatangan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Tandatangan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td class="pull-left" colspan="2">Nama Pemeriksa :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Nama Pemeriksa :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Nama Pemverifikasi :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Nama Pemverifikasi :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td class="pull-left" colspan="2">Jawatan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Jawatan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Jawatan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Jawatan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td class="pull-left" colspan="2">Jabatan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Jabatan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Jabatan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Jabatan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td class="pull-left" colspan="2">Tarikh Pelantikan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Tarikh Pelantikan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Tarikh Pelantikan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Tarikh Pelantikan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td class="pull-left" colspan="2">Tarikh Pemeriksaan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Tarikh Pemeriksaan :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Tarikh Verifikasi :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                    <td class="pull-left" colspan="2">Tarikh Verifikasi :</td>
                    <td class="pull-left" colspan="4">&nbsp;</td>
                </tr>
            </tbody>
        </table><br>
  <!--       <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
            <tbody>
                <tr>
                    <td class="" colspan="2">(Tandatangan Pegawai)</td>
                    <td class="" colspan="3">&nbsp;</td>
                    <td class="" colspan="2">(Tandatangan Ketua Jabatan)</td>
                    <td class="" colspan="3"> </td>
                </tr>
                <tr>
                    <td class="" colspan="2">Nama :</td>
                    <td class="" colspan="3">&nbsp;</td>
                    <td class="" colspan="2">Nama :</td>
                    <td class="" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td class="" colspan="2">Jawatan :</td>
                    <td class="" colspan="3">&nbsp;</td>
                    <td class="" colspan="2">Jawatan :</td>
                    <td class="" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td class="" colspan="2">Tarikh :</td>
                    <td class="" colspan="3">&nbsp;</td>
                    <td class="" colspan="2">Tarikh :</td>
                    <td class="" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td class="" colspan="5" style="width: 90%">*Nota Tandatangan Pegawai boleh ditandatangani oleh Ketua Bahagian/Seksyen/Unit.</td>
                    <td class="" colspan="5" style="width: 10%">Cop Kementerian/Jabatan:
                    <br>* Sila potong yang berkenaan
                    </td>
                </tr>
            </tbody>
        </table>
 -->
                <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
            <tbody>
                <tr>
                    <td class="" style="width: 20%">(Tandatangan Pegawai)</td>
                    <td class="" style="width: 30%">&nbsp;</td>
                    <td class=""  style="width: 20%">(Tandatangan Ketua Jabatan)</td>
                    <td class=""  style="width: 30%"> </td>
                </tr>
                <tr>
                    <td class="" >Nama :</td>
                    <td class="" >&nbsp;</td>
                    <td class="" >Nama :</td>
                    <td class="" >&nbsp;</td>
                </tr>
                <tr>
                    <td class="" >Jawatan :</td>
                    <td class="" >&nbsp;</td>
                    <td class="" >Jawatan :</td>
                    <td class="" >&nbsp;</td>
                </tr>
                <tr>
                    <td class="" >Tarikh :</td>
                    <td class="" >&nbsp;</td>
                    <td class="" >Tarikh :</td>
                    <td class="" >&nbsp;</td>
                </tr>
                <tr>
                    <td class=""  colspan="2">*Nota Tandatangan Pegawai boleh ditandatangani oleh Ketua Bahagian/Seksyen/Unit.</td>
                    <td class="nowrap"  colspan="2">Cop Kementerian/Jabatan:
                    <br>* Sila potong yang berkenaan
                    </td>
                </tr>
            </tbody>
        </table>

        <p>Nota: Ruang Tandatangan boleh di lampiran terakhir </p>


    </div>
</div>
