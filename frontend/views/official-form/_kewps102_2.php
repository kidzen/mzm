
<tbody>
    <?php foreach ($items as $item) { ?>
        <tr>
            <td class="text-center" style="width:10%"><?= $item['inventory']['CODE_NO'] ?></td>
            <td class="nowrap" style="font-size:12;width:40%" colspan="2"><?= mb_strimwidth($item['inventory']['DESCRIPTION'],0,35,"...",'UTF-8') ?></td>
            <td class="text-center border-right-bold" style="width:7%"><?= $item['RQ_QUANTITY'] ?></td>
            <td class="text-center" style="width:7%"><?= $item['inventory']['CARD_NO'] ?></td>
            <td class="text-center" style="width:7%"><?= $item['CURRENT_BALANCE'] ?></td>
            <td class="text-center" style="width:7%"><?= $item['APP_QUANTITY'] ?></td>
            <td class="text-center" style="width:7%"><?= $item['UNIT_PRICE'] / $item['APP_QUANTITY'] ?></td>
            <td class="text-center " style="width:7%;"><?= $item['UNIT_PRICE'] ?></td>
            <td class="text-center" style="width:8%"><?= $item['APP_QUANTITY'] ?></td>
            <td class="text-center" style="width:15%"></td>
        </tr>
    <?php } ?>
    <?php for ($j = sizeof($items); $j < 3; $j++) { ?>
        <tr>
            <td class="col-1 " style="width:10%"><br></td>
            <td class="col-2 " style="width:40%" colspan="2"> </td>
            <td class="col-3 border-right-bold" style="width:7%"> </td>
            <td class="col-4 " style="width:7%"> </td>
            <td class="col-5 " style="width:7%"> </td>
            <td class="col-5 " style="width:7%"> </td>
            <td class="col-5 " style="width:7%"> </td>
            <td class="no-border-right text-center " style="width:7%;"> </td>
            <td class="col-9 " style="width:8%"> </td>
            <td class="col-10 " style="width:15%"></td>
        </tr>
    <?php } ?>

<tbody>
