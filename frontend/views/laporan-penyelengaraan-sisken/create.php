<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LaporanPenyelengaraanSisken */

$this->title = Yii::t('app', 'Create Laporan Penyelengaraan Sisken');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laporan Penyelengaraan Siskens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laporan-penyelengaraan-sisken-create">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
