<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $transaction common\models\InventoryItems */
$this->title = Yii::t('app', 'Kemasukan Stok');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estor-inventories-form">


    <?=
    $this->render('_form', [
        'transaction' => $transaction,
        'checkIn' => $checkIn,
        'inventory' => $inventory,
        'items' => $items,
        'inventoriesArray' => $inventoriesArray,
        'vendorsArray' => $vendorsArray,
    ])
    ?>


</div>            

