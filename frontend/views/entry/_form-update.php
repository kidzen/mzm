<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use bajadev\dynamicform\DynamicFormWidget;

//die();
/* @var $this yii\web\View */
/* @var $transaction common\models\Transactions */
//die();
$this->title = Yii::t('app', 'Update Transactions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//var_dump($transaction->attributes);
////var_dump($inventory->attributes);
//var_dump($checkIn->attributes);
//var_dump($items[0]->attributes);
//die();
?>
<div class="transactions-create">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <!--<div class='row'>-->
    <div class="panel panel-primary">
        <div class="panel-heading"><h4><i class="fa fa-upload"></i> <?= $this->title ?></h4></div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($inventory, 'DESCRIPTION')->textInput(['readOnly' => true]);
                    ?>                    
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($checkIn, 'VENDOR_ID')->widget(kartik\select2\Select2::className(), [
                        'data' => $vendorsArray,
                        'theme' => 'default',
                        'options' => ['placeholder' => 'Select a vendor ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]);
                    ?>

                </div>
                <div class="col-sm-4">
                    <?= $form->field($inventory, 'QUANTITY')->textInput(['readOnly' => true]); ?>
                </div>
            </div>
            <!--////////////////////////////////estor items/////////////////////////////////-->

            <?php if ($checkIn->APPROVED == 2) { ?>

                <?php
                DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
//                    'limit' => 4, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $items[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'ID',
                        'INVENTORY_ID',
                        'RQ_QUANTITY',
                    ],
                ]);

//            var_dump($transaction->APPROVED);die();
                ?>

                <div class="container-items"><!-- widgetContainer -->
                    <?php foreach ($items as $i => $item): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Items</h3>
                                <div class="pull-right">
                                    <button type = "button" class = "add-item btn btn-success btn-xs"><i class = "glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                // necessary for update action.
                                if (!$item->isNewRecord) {
                                    echo Html::activeHiddenInput($item, "[{$i}]ID");
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $form->field($item, "[{$i}]SKU")->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($item, "[{$i}]UNIT_PRICE")->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php DynamicFormWidget::end(); ?>
            <?php } ?>

            <div class="form-group">
                <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
                <?= Html::submitButton($checkIn->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $checkIn->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

        </div>
        <!--</div>-->


    </div>
    <?php ActiveForm::end(); ?>

</div>
