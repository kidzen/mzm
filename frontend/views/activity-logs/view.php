<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ActivityLogs */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Activity Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info activity-logs-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>activity-logs : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'ID',
            'USER_ID',
            'REMOTE_IP',
            'ACTION',
            'CONTROLLER',
            'PARAMS',
            'ROUTE',
            'STATUS',
            'MESSAGES',
            'CREATED_AT',
    ],
    ]) ?>

</div>
