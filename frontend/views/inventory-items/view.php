<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

//var_dump(Yii::$app->request->referrer);
//var_dump(preg_match('/*inventory-items*/', Yii::$app->request->referrer));
//die();
/* @var $this yii\web\View */
/* @var $model common\models\InventoryItems */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info inventory-items-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>inventory-items : <?= Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Kembali'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Cetak Barkod Inventori & SKU'), ['/inventory-items/print-sku-by-id','id'=>$model->ID,'printInventoryCode'=>true], ['class' => 'btn btn-info','target'=>'_blank']) ?>
            <?= Html::a(Yii::t('app', 'Cetak Barkod Inventori'), ['/inventories/print-barcode','id'=>$model->inventory->ID], ['class' => 'btn btn-info','target'=>'_blank']) ?>
            <?= Html::a(Yii::t('app', 'Cetak Barkod SKU'), ['/inventory-items/print-sku-by-id','id'=>$model->ID,'printInventoryCode'=>false], ['class' => 'btn btn-info','target'=>'_blank']) ?>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID',
                'INVENTORY_ID',
                'CHECKIN_TRANSACTION_ID',
                'CHECKOUT_TRANSACTION_ID',
                'SKU',
                'UNIT_PRICE',
                'CREATED_AT',
                'UPDATED_AT',
                'CREATED_BY',
                'UPDATED_BY',
                [
                    'attribute' => 'DELETED',
                    'vAlign' => 'middle',
                    'value' => $model->DELETED == 1 ? 'Deleted' : 'Active',
                ],
                'DELETED_AT',
            ],
        ])
        ?>

    </div>
