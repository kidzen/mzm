<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InventoryItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventory-items-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
            <!--            <div class="col-md-4">
            <?= $form->field($model, 'INVENTORY_ID')->textInput() ?>
                        </div>-->
            <div class="col-md-4">
                <?= $form->field($model, 'CHECKIN_TRANSACTION_ID')->textInput(['readOnly' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'CHECKOUT_TRANSACTION_ID')->textInput(['readOnly' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'SKU')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'UNIT_PRICE')->textInput(['maxlength' => true]) ?>
            </div>

        </div>
        <div>
            <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
