<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
//var_dump($username);die();

//$counter1 = 1;
//$datas1 = ['data10', 'data11'];
//
//$counter2 = 2;
//$datas2 = ['data20', 'data21'];
//
//$counter3 = 3;
//$datas3 = ['data30', 'data31'];
//
//$counter4 = 4;
//$datas4 = ['data40', 'data41'];
//
//$class1 = '';

$avatar = isset(Yii::$app->user->avatar) ? Yii::getAlias('@web/') . Yii::$app->user->avatar : $directoryAsset . '/img/tiada_gambar.jpg';
?>
<?php
if (isset(Yii::$app->user->avatar)) {
    echo Yii::getAlias('@web/') . Yii::$app->user->avatar;
} else {
    echo $directoryAsset . '/img/tiada_gambar.jpg';
}
?>
<header class="main-header">
    <?PHP // ECHO Html::a('<span class="logo-mini">  <img src="' . $directoryAsset . '/img/favicon-32x32.png"  alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/> </span><span class="logo-lg"><img src="' . $directoryAsset . '/img/favicon-32x32.png" style="padding-right: 10px" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai">' . Yii::$app->name . '</></span>', Yii::$app->homeUrl, ['class' => 'logo'])    ?>
    <?php Html::a('<span class="logo-mini">  <img src="' . $directoryAsset . '/img/mzm.png"  alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/> </span><span class="logo-lg"><img src="' . $directoryAsset . '/img/mzm.png" style="padding-right: 10px" alt="Muzzam Teknologi Sdn Bhd"></></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <?= Html::a('<span class="logo-mini">MZM</span><span class="logo-lg">Muzzam-Tek</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">


                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $avatar ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $avatar ?>" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?= Yii::$app->user->name ?>
                                <br> <?= $roles ?>
                                <!--<small>Member since Nov. 2012</small>-->
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!--                        <li class="user-body">
                                                    <div class="col-xs-4 text-center">
                                                        <a href="#">Followers</a>
                                                    </div>
                                                    <div class="col-xs-4 text-center">
                                                        <a href="#">Sales</a>
                                                    </div>
                                                    <div class="col-xs-4 text-center">
                                                        <a href="#">Friends</a>
                                                    </div>
                                                </li>-->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <?php if (isset(Yii::$app->user->id)) { ?>
                                <div class="pull-left">
                                    <?=
                                    Html::a(
                                            'Profil', ['people/view', 'id' => Yii::$app->user->id], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                    ?>
                                </div>

                                <div class="pull-right">
                                    <?=
                                    Html::a(
                                            'Log Keluar', ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                    ?>
                                </div>
                            <?php } else { ?>
                                <div class="pull-right">
                                    <?=
                                    Html::a(
                                            'Log Masuk', ['/site/login'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                    ?>
                                </div>
                            <?php } ?>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <!--                                <li>
                                                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                                                </li>-->
            </ul>
        </div>
    </nav>
</header>
