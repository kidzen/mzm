<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehicle-list-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
            <!--            <div class="col-md-4">
            <?= $form->field($model, 'ID')->textInput(['maxlength' => true]) ?>
                        </div>-->
            <div class="col-md-4">
                <?= $form->field($model, 'REG_NO')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'MODEL')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'TYPE')->textInput(['maxlength' => true]) ?>
            </div>
            <?php if (Yii::$app->user->isAdmin && !$model->isNewRecord) { ?>
                <div class="col-md-4">
                    <?= $form->field($model, 'DELETED')->textInput() ?>
                </div>
            <?php } ?>

        </div>
        <div>
            <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
