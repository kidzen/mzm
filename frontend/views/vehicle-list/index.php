<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VehicleListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Vehicle Lists');
$this->params['breadcrumbs'][] = $this->title;

$title = empty($this->title) ? Yii::t('app', 'Report') : $this->title . ' Report';
$pdfHeader = [
    'L' => [
        'content' => Yii::t('app', 'Sistem e-Store MPSP'),
        'font-size' => 8,
        'font-weight' => '600',
        'color' => '#333333'
    ],
    'C' => [
        'content' => $title,
        'font-size' => 16,
        'font-weight' => 'bold',
        'color' => '#333333'
    ],
    'R' => [
        'content' => Yii::t('app', 'Generated') . ': ' . date("D, d-M-Y H:i:s"),
        'font-size' => 8,
        'color' => '#333333'
    ]
];
$pdfFooter = [
    'L' => [
        'content' => Yii::t('app', "Copyright © Muzzam Teknologi Sdn Bhd"),
        'font-size' => 8,
        'font-style' => 'B',
        'color' => '#999999'
    ],
    'R' => [
        'content' => '[ {PAGENO} ]',
        'font-size' => 10,
        'font-style' => 'B',
        'font-family' => 'serif',
        'color' => '#333333'
    ],
    'line' => true,
];
$pdf = [
    'label' => Yii::t('app', 'PDF'),
    'icon' => 'file-pdf-o',
    'iconOptions' => ['class' => 'text-danger'],
    'showHeader' => true,
    'showPageSummary' => true,
    'showFooter' => true,
    'showCaption' => true,
    'filename' => $this->title . '_' . date('d-M-Y'),
    'alertMsg' => Yii::t('app', 'The PDF export file will be generated for download.'),
    'options' => ['title' => Yii::t('app', 'Portable Document Format')],
    'mime' => 'application/pdf',
    'config' => [
        'mode' => 'c',
        'format' => 'A4-L',
//        'destination' => kartik\mpdf\Pdf::DEST_BROWSER,
        'destination' => 'I',
        'marginTop' => 20,
        'marginBottom' => 20,
        'cssInline' => '.kv-wrap{padding:20px;}' .
        '.kv-align-center{text-align:center;}' .
        '.kv-align-left{text-align:left;}' .
        '.kv-align-right{text-align:right;}' .
        '.kv-align-top{vertical-align:top!important;}' .
        '.kv-align-bottom{vertical-align:bottom!important;}' .
        '.kv-align-middle{vertical-align:middle!important;}' .
        '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
        '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
        '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
        'methods' => [
            'SetHeader' => [
                ['odd' => $pdfHeader, 'even' => $pdfHeader]
            ],
            'SetFooter' => [
                ['odd' => $pdfFooter, 'even' => $pdfFooter]
            ],
        ],
        'options' => [
            'title' => $title,
            'subject' => Yii::t('app', 'PDF export generated by kartik-v/yii2-grid extension'),
            'keywords' => Yii::t('app', 'krajee, grid, export, yii2-grid, pdf')
        ],
        'contentBefore' => '',
        'contentAfter' => ''
    ]
];
?>
<div class="vehicle-list-index">

    <?php Pjax::begin(); ?>            <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
//        'tableOptions' => ['class' => 'table-responsive'],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            ['content' =>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => Yii::t('app', 'Create vehicle-list'),])//. ' ' .
            //                Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) //. ' ' .
            //                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => Yii::t('app', 'Add Book'), 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' ' .
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
        'exportConfig' => [
            'pdf' => $pdf,
            'csv' => '{csv}',
            'xls' => '{xls}',
        ],
        'export' => [
            'fontAwesome' => true,
//            'target' => '_self',
            'target' => '_blank',
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //        'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => $this->title,
        ],
//        'perfectScrollbar' => true
        'rowOptions' => ['hAlign' => 'center', 'vAlign' => 'middle'],
        'resizableColumns' => true,
        'persistResize' => false,
        'responsiveWrap' => false,
        //        'exportConfig' => $exportConfig,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'ID',
                'vAlign' => 'middle',
//                'value'=>'hai',
                'hAlign' => 'center',
//                'format' => 'raw',
            ],
            [
                'attribute' => 'REG_NO',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'MODEL',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'TYPE',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'CREATED_AT',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            /* [
              'attribute'=>'UPDATED_AT',
              ], */
            /* [
              'attribute'=>'CREATED_BY',
              ], */
            /* [
              'attribute'=>'UPDATED_BY',
              ], */
            /* [
              'attribute'=>'DELETED',
              ], */
            /* [
              'attribute'=>'DELETED_AT',
              ], */
            [
                'class' => 'kartik\grid\ActionColumn',
                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,
                'template' => '{view}{update}{delete}{recover}',
                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
                'buttons' => [
                    'recover' => function ($url, $model) {
                        if ($model->DELETED === 1) {
                            return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip']);
                        }
                    },
                            'delete' => function ($url, $model) {
                        if ($model->DELETED === 0) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                                        'data-method' => 'post']);
                        }
                    },
                        ],
                    ],
                    [
                        'header' => 'Permanent Delete',
                        'class' => 'kartik\grid\ActionColumn',
                        'template' => '{delete-permanent}',
                        'buttons' => [
                            'delete-permanent' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                                            'title' => Yii::t('yii', 'Hard Delete'), 'data-toggle' => 'tooltip'
                                ]);
                            }
                                ],
                                'visible' => Yii::$app->user->isAdmin,
                            ],
                        ],
                    ]);
                    ?>
                    <?php Pjax::end(); ?></div>
