<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MpspStaff */

$this->title = 'Create Mpsp Staff';
$this->params['breadcrumbs'][] = ['label' => 'Mpsp Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpsp-staff-create">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
