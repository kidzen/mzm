<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MpspStaff */

$this->title = 'Update Mpsp Staff: ' . $model->NO_PEKERJA;
$this->params['breadcrumbs'][] = ['label' => 'Mpsp Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NO_PEKERJA, 'url' => ['view', 'id' => $model->NO_PEKERJA]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mpsp-staff-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
