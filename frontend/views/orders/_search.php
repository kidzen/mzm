<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'TRANSACTION_ID') ?>

    <?= $form->field($model, 'ORDER_DATE') ?>

    <?= $form->field($model, 'ORDERED_BY') ?>

    <?= $form->field($model, 'ORDER_NO') ?>

    <?php // echo $form->field($model, 'APPROVED') ?>

    <?php // echo $form->field($model, 'APPROVED_BY') ?>

    <?php // echo $form->field($model, 'APPROVED_AT') ?>

    <?php // echo $form->field($model, 'VEHICLE_ID') ?>

    <?php // echo $form->field($model, 'REQUIRED_DATE') ?>

    <?php // echo $form->field($model, 'CHECKOUT_DATE') ?>

    <?php // echo $form->field($model, 'CHECKOUT_BY') ?>

    <?php // echo $form->field($model, 'CREATED_AT') ?>

    <?php // echo $form->field($model, 'UPDATED_AT') ?>

    <?php // echo $form->field($model, 'CREATED_BY') ?>

    <?php // echo $form->field($model, 'UPDATED_BY') ?>

    <?php // echo $form->field($model, 'DELETED') ?>

    <?php // echo $form->field($model, 'DELETED_AT') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
