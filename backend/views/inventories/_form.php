<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Inventories $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="inventories-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Inventories',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]
    );
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'ID')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'CATEGORY_ID')->textInput() ?>
			<?= $form->field($model, 'CARD_NO')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'CODE_NO')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'DESCRIPTION')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'QUANTITY')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'MIN_STOCK')->textInput() ?>
			<?= $form->field($model, 'LOCATION')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'APPROVED')->textInput() ?>
			<?= $form->field($model, 'APPROVED_AT')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'APPROVED_BY')->textInput() ?>
			<?= $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true]) ?>
			<?= // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::activeField
$form->field($model, 'CREATED_BY')->dropDownList(
    \yii\helpers\ArrayHelper::map(common\models\People::find()->all(), 'ID', 'ID'),
    ['prompt' => Yii::t('app', 'Select')]
); ?>
			<?= $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true]) ?>
			<?= // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::activeField
$form->field($model, 'UPDATED_BY')->dropDownList(
    \yii\helpers\ArrayHelper::map(common\models\People::find()->all(), 'ID', 'ID'),
    ['prompt' => Yii::t('app', 'Select')]
); ?>
			<?= $form->field($model, 'DELETED')->textInput() ?>
			<?= $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => Yii::t('app', StringHelper::basename('common\models\Inventories')),
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

