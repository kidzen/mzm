<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->PASSWORD_RESET_TOKEN]);
?>
Hello <?= $user->USERNAME ?>,

Follow the link below to reset your password:

<?= $resetLink ?>
