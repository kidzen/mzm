<?php
return [
    'adminEmail' => 'admin@mpsp.gov.my',
    'supportEmail' => 'support@mpsp.gov.my',
    'user.passwordResetTokenExpire' => 3600,
];
