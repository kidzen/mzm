<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "IC_PRICE_CHECK".
 *
 * @property string $IV_ID
 * @property string $IC_ID
 * @property string $IC_TOTAL_PRICE
 * @property string $ACTUAL_TOTAL_PRICE
 */
class InventoryCheckinPriceCheck extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'IC_PRICE_CHECK';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IV_ID', 'IC_ID', 'IC_TOTAL_PRICE', 'ACTUAL_TOTAL_PRICE'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IV_ID' => Yii::t('app', 'Iv  ID'),
            'IC_ID' => Yii::t('app', 'Ic  ID'),
            'IC_TOTAL_PRICE' => Yii::t('app', 'Ic  Total  Price'),
            'ACTUAL_TOTAL_PRICE' => Yii::t('app', 'Actual  Total  Price'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
