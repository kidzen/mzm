<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InventoryItems;

/**
 * InventoryItemsSearch represents the model behind the search form about `common\models\InventoryItems`.
 */
class EntrySearch extends InventoryItems {

    /**
     * @inheritdoc
     */
    public $INVENTORY_DETAILS;

    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
//            'checkInTransaction.CHECK_DATE', 'checkOutTransaction.CHECK_DATE', 'INVENTORY_DETAILS']);
        return array_merge(parent::attributes(), ['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
            'transactionIn.CHECK_DATE', 'checkOutTransaction.ORDER_DATE', 'INVENTORY_DETAILS']);
    }

    public function rules() {
        return [
            [['ID', 'UNIT_PRICE'], 'number'],
//            [['DELETED'], 'default', 'value'=>0],
            [['INVENTORY_ID', 'CHECKIN_TRANSACTION_ID', 'CHECKOUT_TRANSACTION_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['SKU', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT','INVENTORY_DETAILS'], 'safe'],
//            [['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO', 'checkInTransaction.CHECK_DATE',
//            'checkOutTransaction.CHECK_DATE', 'INVENTORY_DETAILS'], 'safe'],
//            [['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO', 'checkInTransaction.CHECK_DATE',
//            'checkOutTransaction.CHECK_DATE', 'INVENTORY_DETAILS'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = InventoryItems::find();
        $query->joinWith(['category']);
//        $query->joinWith(['checkInTransaction']);
//        $query->joinWith(['checkOutTransaction']);
        $query->joinWith(['transactionIn']);
        $query->joinWith(['transactionOut']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
//        $dataProvider->sort->defaultOrder = ['CREATED_AT' => SORT_DESC];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['ID'] = [
            'asc' => ['INVENTORY_ITEMS.ID' => SORT_ASC],
            'desc' => ['INVENTORY_ITEMS.ID' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['CREATED_AT'] = [
            'asc' => ['INVENTORY_ITEMS.CREATED_AT' => SORT_ASC],
            'desc' => ['INVENTORY_ITEMS.CREATED_AT' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['INVENTORY_DETAILS'] = [
            'asc' => ['INVENTORIES.CODE_NO' => SORT_ASC],
            'desc' => ['INVENTORIES.CODE_NO' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['category.NAME'] = [
            'asc' => ['CATEGORIES.NAME' => SORT_ASC],
            'desc' => ['CATEGORIES.NAME' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['inventory.QUANTITY'] = [
            'asc' => ['INVENTORIES.QUANTITY' => SORT_ASC],
            'desc' => ['INVENTORIES.QUANTITY' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['transactionIn.CHECK_DATE'] = [
            'asc' => ['TRANSACTIONS.CHECK_DATE' => SORT_ASC],
            'desc' => ['TRANSACTIONS.CHECK_DATE' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['checkOutTransaction.ORDER_DATE'] = [
            'asc' => ['ORDERS.ORDER_DATE' => SORT_ASC],
            'desc' => ['ORDERS.ORDER_DATE' => SORT_DESC],
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'INVENTORY_ITEMS.ID' => $this->ID,
            'INVENTORY_ID' => $this->INVENTORY_ID,
            'CHECKIN_TRANSACTION_ID' => $this->CHECKIN_TRANSACTION_ID,
            'CHECKOUT_TRANSACTION_ID' => $this->CHECKOUT_TRANSACTION_ID,
            'UNIT_PRICE' => $this->UNIT_PRICE,
            'INVENTORY_ITEMS.CREATED_AT' => $this->CREATED_AT,
            'UPDATED_AT' => $this->UPDATED_AT,
            'CREATED_BY' => $this->CREATED_BY,
            'UPDATED_BY' => $this->UPDATED_BY,
            'DELETED' => $this->DELETED,
            'DELETED_AT' => $this->DELETED_AT,
//            'inventory.DESCRIPTION' => $this>getAttribute('inventory.DESCRIPTION'),
        ]);
//        var_dump($this->getAttributes());die();
        $query->andFilterWhere(['like', 'SKU', $this->SKU]);
        $query->andFilterWhere(['LIKE', 'CATEGORIES.NAME', $this->getAttribute('category.NAME')]);
        $query->andFilterWhere(['LIKE', 'INVENTORIES.QUANTITY', $this->getAttribute('inventory.QUANTITY')]);
//        $query->andFilterWhere(['LIKE', 'inventory.DESCRIPTION', $this->getAttribute('inventory.DESCRIPTION')]);
//        $query->orFilterWhere(['LIKE', 'inventory.CARD_NO', $this->getAttribute('INVENTORY_DETAILS')]);
//        $query->orFilterWhere(['LIKE', 'inventory.CODE_NO', $this->getAttribute('INVENTORY_DETAILS')]);
        $query->orFilterWhere(['LIKE', 'INVENTORIES.CARD_NO', $this->INVENTORY_DETAILS]);
        $query->orFilterWhere(['LIKE', 'INVENTORIES.CODE_NO', $this->INVENTORY_DETAILS]);
        $query->orFilterWhere(['LIKE', 'INVENTORIES.DESCRIPTION', $this->INVENTORY_DETAILS]);

        return $dataProvider;
    }

}
