<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Approval;

/**
 * ApprovalSearch represents the model behind the search form about `common\models\Approval`.
 */
class ApprovalSearch extends Approval {

    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
//            'checkInTransaction.CHECK_DATE', 'checkOutTransaction.CHECK_DATE', 'INVENTORY_DETAILS']);
        return array_merge(parent::attributes(), [
            'category.NAME',
            'order.ORDER_NO', 'order.ORDER_DATE', 'order.REQUIRED_DATE', 'order.APPROVED',
            'arahanKerja.NO_KERJA',
            'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
            'INVENTORY_DETAILS', 'USAGE_DETAIL'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
//            [['ID', 'ID_INVENTORY_ITEMS', 'ID_INVENTORIES', 'ID_CATEGORIES', 'ID_INVENTORIES_CHECKIN', 'ID_ORDER_ITEMS', 'ID_ORDERS',
//                'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'TOTAL_PRICE', 'UNIT_PRICE'], 'number'],
            [['ID', 'AVAILABLE_PER_ORDER', 'ID_INVENTORY_ITEMS', 'ID_INVENTORIES', 'ID_CATEGORIES', 'ID_INVENTORIES_CHECKIN',
            'ID_ORDER_ITEMS', 'ID_ORDERS', 'UNIT_PRICE', 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'TOTAL_PRICE'], 'number'],
            [['SKU', 'ORDER_NO', 'ITEMS_CATEGORY_NAME', 'ITEMS_INVENTORY_NAME', 'ORDER_DATE', 'REQUIRED_DATE'], 'safe'],
            [['APPROVED'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ordersId) {
        $query = Approval::find();
        $query->where(['ID_ORDERS' => $ordersId]);
//        $query->andWhere(['INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' => null]);
        $query->joinWith('inventoryItem');
//        $query->joinWith('category');
        $query->joinWith('inventory');
//        $query->joinWith('orderItem');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider->sort->attributes['UNIT_PRICE'] = ['asc' => ['APPROVAL.UNIT_PRICE' => SORT_ASC], 'desc' => ['APPROVAL.UNIT_PRICE' => SORT_DESC],];
        $dataProvider->sort->attributes['inventory.QUANTITY'] = ['asc' => ['INVENTORY.QUANTITY' => SORT_ASC], 'desc' => ['INVENTORY.QUANTITY' => SORT_DESC],];
        // grid filtering conditions
        $query->andFilterWhere([
            'APPROVAL.ID' => $this->ID,
            'APPROVAL.ID_INVENTORY_ITEMS' => $this->ID_INVENTORY_ITEMS,
            'APPROVAL.ID_INVENTORIES' => $this->ID_INVENTORIES,
            'APPROVAL.ID_CATEGORIES' => $this->ID_CATEGORIES,
            'APPROVAL.ID_INVENTORIES_CHECKIN' => $this->ID_INVENTORIES_CHECKIN,
            'APPROVAL.ID_ORDER_ITEMS' => $this->ID_ORDER_ITEMS,
            'APPROVAL.ID_ORDERS' => $this->ID_ORDERS,
            'APPROVAL.RQ_QUANTITY' => $this->RQ_QUANTITY,
            'APPROVAL.APP_QUANTITY' => $this->APP_QUANTITY,
            'APPROVAL.APPROVED' => $this->APPROVED,
            'APPROVAL.CURRENT_BALANCE' => $this->CURRENT_BALANCE,
            'APPROVAL.UNIT_PRICE' => $this->UNIT_PRICE,
            'APPROVAL.TOTAL_PRICE' => $this->TOTAL_PRICE,
        ]);

        $query->andFilterWhere(['like', 'APPROVAL.SKU', $this->SKU]);
        $query->andFilterWhere(['like', 'APPROVAL.ORDER_NO', $this->ORDER_NO]);
        $query->andFilterWhere(['like', 'APPROVAL.ITEMS_CATEGORY_NAME', $this->ITEMS_CATEGORY_NAME]);
        $query->andFilterWhere(['like', 'ITEMS_INVENTORY_NAME', $this->ITEMS_INVENTORY_NAME]);
        $query->andFilterWhere(['like', 'ORDER_DATE', $this->ORDER_DATE]);
        $query->andFilterWhere(['like', 'REQUIRED_DATE', $this->REQUIRED_DATE]);
        $query->andFilterWhere(['like', 'REQUIRED_DATE', $this->REQUIRED_DATE]);
        ;

        return $dataProvider;
    }

}
