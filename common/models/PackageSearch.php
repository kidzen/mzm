<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Package;

/**
 * PackageSearch represents the model behind the search form about `common\models\Package`.
 */
class PackageSearch extends Package {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ID', 'CREATED_BY', 'UPDATED_BY'], 'number'],
            [['ORDER_ID', 'PACKAGE_BY', 'DELETED'], 'integer'],
            [['DETAIL', 'DELIVERY', 'PACKAGE_DATE', 'DELETED_AT', 'CREATED_AT', 'UPDATED_AT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Package::find();
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['PACKAGE.DELETED' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'ORDER_ID' => $this->ORDER_ID,
            'PACKAGE_BY' => $this->PACKAGE_BY,
            'DELETED' => $this->DELETED,
            'DELETED_AT' => $this->DELETED_AT,
            'CREATED_AT' => $this->CREATED_AT,
            'UPDATED_AT' => $this->UPDATED_AT,
            'CREATED_BY' => $this->CREATED_BY,
            'UPDATED_BY' => $this->UPDATED_BY,
        ]);

        $query->andFilterWhere(['like', 'DETAIL', $this->DETAIL])
                ->andFilterWhere(['like', 'DELIVERY', $this->DELIVERY])
                ->andFilterWhere(['like', 'PACKAGE_DATE', $this->PACKAGE_DATE]);

        return $dataProvider;
    }

}
