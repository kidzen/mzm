<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InventoriesCheckIn;

/**
 * InventoryItemsSearch represents the model behind the search form about `common\models\InventoryItems`.
 */
class EntryTransactionSearch extends InventoriesCheckIn {

    /**
     * @inheritdoc
     */
    public $INVENTORY_DETAILS;

    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
//            'checkInTransaction.CHECK_DATE', 'checkOutTransaction.CHECK_DATE', 'INVENTORY_DETAILS']);
        return array_merge(parent::attributes(), ['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
            'INVENTORY_DETAILS', 'creator.USERNAME', 'updator.USERNAME']);
    }

    public function rules() {
        return [
//            [['ID', 'UNIT_PRICE'], 'number'],
////            [['DELETED'], 'default', 'value'=>0],
//            [['INVENTORY_ID', 'CHECKIN_TRANSACTION_ID', 'CHECKOUT_TRANSACTION_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
//            [['SKU', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
//            [['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO', 'checkInTransaction.CHECK_DATE',
//            'checkOutTransaction.CHECK_DATE', 'INVENTORY_DETAILS'], 'safe'],
//            [['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO', 'checkInTransaction.CHECK_DATE',
//            'checkOutTransaction.CHECK_DATE', 'INVENTORY_DETAILS'], 'safe'],
            [['ID', 'ITEMS_QUANTITY', 'ITEMS_TOTAL_PRICE'], 'number'],
            [['TRANSACTION_ID', 'INVENTORY_ID', 'VENDOR_ID', 'CHECK_BY', 'APPROVED', 'APPROVED_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CHECK_DATE', 'APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT', 'INVENTORY_DETAILS', 'inventory.QUANTITY','creator.USERNAME'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = InventoriesCheckIn::find();
        $query->joinWith(['category']);
        $query->joinWith(['transaction']);
        $query->joinWith(['creator']);
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['INVENTORIES_CHECKIN.DELETED' => 0]);
        }

//        $query->orFilterWhere(['like', 'INVENTORIES_CHECKIN.DELETED', 0]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['CREATED_AT' => SORT_DESC];
//        if (!\Yii::$app->user->isAdmin) {
//            $query->andFilterWhere(['like', 'INVENTORIES_CHECKIN.DELETED', 0]);
//        }
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['CREATED_AT'] = [
            'asc' => ['INVENTORIES_CHECKIN.CREATED_AT' => SORT_ASC],
            'desc' => ['INVENTORIES_CHECKIN.CREATED_AT' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['UPDATED_AT'] = [
            'asc' => ['INVENTORIES_CHECKIN.UPDATED_AT' => SORT_ASC],
            'desc' => ['INVENTORIES_CHECKIN.UPDATED_AT' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['creator.USERNAME'] = [
            'asc' => ['PEOPLE.USERNAME' => SORT_ASC],
            'desc' => ['PEOPLE.USERNAME' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['updator.USERNAME'] = [
            'asc' => ['PEOPLE.USERNAME' => SORT_ASC],
            'desc' => ['PEOPLE.USERNAME' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['ID'] = [
            'asc' => ['INVENTORIES_CHECKIN.ID' => SORT_ASC],
            'desc' => ['INVENTORIES_CHECKIN.ID' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['INVENTORY_DETAILS'] = [
            'asc' => ['INVENTORIES.CODE_NO' => SORT_ASC],
            'desc' => ['INVENTORIES.CODE_NO' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['category.NAME'] = [
            'asc' => ['CATEGORIES.NAME' => SORT_ASC],
            'desc' => ['CATEGORIES.NAME' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['inventory.QUANTITY'] = [
            'asc' => ['INVENTORIES.QUANTITY' => SORT_ASC],
            'desc' => ['INVENTORIES.QUANTITY' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['CHECK_DATE'] = [
            'asc' => ['INVENTORIES_CHECKIN.CHECK_DATE' => SORT_ASC],
            'desc' => ['INVENTORIES_CHECKIN.CHECK_DATE' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['CHECK_BY'] = [
            'asc' => ['INVENTORIES_CHECKIN.CHECK_BY' => SORT_ASC],
            'desc' => ['INVENTORIES_CHECKIN.CHECK_BY' => SORT_DESC],
        ];
        // grid filtering conditions
        $query->andFilterWhere([
            'INVENTORIES_CHECKIN.ID' => $this->ID,
            'INVENTORIES_CHECKIN.TRANSACTION_ID' => $this->TRANSACTION_ID,
            'INVENTORIES_CHECKIN.INVENTORY_ID' => $this->INVENTORY_ID,
            'INVENTORIES_CHECKIN.VENDOR_ID' => $this->VENDOR_ID,
            'INVENTORIES_CHECKIN.ITEMS_QUANTITY' => $this->ITEMS_QUANTITY,
            'INVENTORIES_CHECKIN.ITEMS_TOTAL_PRICE' => $this->ITEMS_TOTAL_PRICE,
            'INVENTORIES_CHECKIN.CHECK_BY' => $this->CHECK_BY,
            'INVENTORIES_CHECKIN.APPROVED' => $this->APPROVED,
            'INVENTORIES_CHECKIN.APPROVED_BY' => $this->APPROVED_BY,
            'INVENTORIES_CHECKIN.APPROVED_AT' => $this->APPROVED_AT,
//            'INVENTORIES_CHECKIN.CREATED_AT' => $this->CREATED_AT,
//            'INVENTORIES_CHECKIN.UPDATED_AT' => $this->UPDATED_AT,
            'INVENTORIES_CHECKIN.CREATED_BY' => $this->CREATED_BY,
            'INVENTORIES_CHECKIN.UPDATED_BY' => $this->UPDATED_BY,
//            'INVENTORIES_CHECKIN.DELETED' => $this->DELETED,
            'INVENTORIES_CHECKIN.DELETED_AT' => $this->DELETED_AT,
        ]);

        $query->andFilterWhere(['like', 'INVENTORIES_CHECKIN.CHECK_DATE', $this->CHECK_DATE]);
        $query->andFilterWhere(['like', 'INVENTORIES_CHECKIN.CREATED_AT', $this->CREATED_AT]);
        $query->andFilterWhere(['like', 'INVENTORIES_CHECKIN.UPDATED_AT', $this->UPDATED_AT]);
        $query->andFilterWhere(['LIKE', 'CATEGORIES.NAME', $this->getAttribute('category.NAME')]);
        $query->andFilterWhere(['LIKE', 'INVENTORIES.QUANTITY', $this->getAttribute('inventory.QUANTITY')]);
        $query->andFilterWhere(['LIKE', 'PEOPLE.USERNAME', $this->getAttribute('creator.USERNAME')]);
//        $query->andFilterWhere(['LIKE', 'INVENTORIES.CARD_NO', $this->getAttribute('INVENTORY_DETAILS')]);
//        $query->orFilterWhere(['LIKE', 'INVENTORIES.CODE_NO', $this->getAttribute('INVENTORY_DETAILS')]);
//        $query->orFilterWhere(['LIKE', 'INVENTORIES.DESCRIPTION', $this->getAttribute('INVENTORY_DETAILS')]);
        $query->orFilterWhere(['LIKE', 'INVENTORIES.CODE_NO', $this->INVENTORY_DETAILS]);
        $query->orFilterWhere(['LIKE', 'INVENTORIES.CARD_NO', $this->INVENTORY_DETAILS]);
        $query->orFilterWhere(['LIKE', 'INVENTORIES.DESCRIPTION', $this->INVENTORY_DETAILS]);

        return $dataProvider;
    }

}
