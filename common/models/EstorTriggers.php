<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "SYS.USER_TRIGGERS".
 *
 * @property string $TRIGGER_NAME
 * @property string $TRIGGER_TYPE
 * @property string $TRIGGERING_EVENT
 * @property string $TABLE_OWNER
 * @property string $BASE_OBJECT_TYPE
 * @property string $TABLE_NAME
 * @property string $COLUMN_NAME
 * @property string $REFERENCING_NAMES
 * @property string $WHEN_CLAUSE
 * @property string $STATUS
 * @property string $DESCRIPTION
 * @property string $ACTION_TYPE
 * @property string $TRIGGER_BODY
 * @property string $CROSSEDITION
 * @property string $BEFORE_STATEMENT
 * @property string $BEFORE_ROW
 * @property string $AFTER_ROW
 * @property string $AFTER_STATEMENT
 * @property string $INSTEAD_OF_ROW
 * @property string $FIRE_ONCE
 * @property string $APPLY_SERVER_ONLY
 */
class EstorTriggers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SYS.USER_TRIGGERS';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TRIGGER_BODY'], 'string'],
            [['TRIGGER_NAME', 'TABLE_OWNER', 'TABLE_NAME'], 'string', 'max' => 30],
            [['TRIGGER_TYPE', 'BASE_OBJECT_TYPE'], 'string', 'max' => 16],
            [['TRIGGERING_EVENT'], 'string', 'max' => 227],
            [['COLUMN_NAME', 'WHEN_CLAUSE', 'DESCRIPTION'], 'string', 'max' => 4000],
            [['REFERENCING_NAMES'], 'string', 'max' => 128],
            [['STATUS'], 'string', 'max' => 8],
            [['ACTION_TYPE'], 'string', 'max' => 11],
            [['CROSSEDITION'], 'string', 'max' => 7],
            [['BEFORE_STATEMENT', 'BEFORE_ROW', 'AFTER_ROW', 'AFTER_STATEMENT', 'INSTEAD_OF_ROW', 'FIRE_ONCE', 'APPLY_SERVER_ONLY'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TRIGGER_NAME' => Yii::t('app', 'Trigger  Name'),
            'TRIGGER_TYPE' => Yii::t('app', 'Trigger  Type'),
            'TRIGGERING_EVENT' => Yii::t('app', 'Triggering  Event'),
            'TABLE_OWNER' => Yii::t('app', 'Table  Owner'),
            'BASE_OBJECT_TYPE' => Yii::t('app', 'Base  Object  Type'),
            'TABLE_NAME' => Yii::t('app', 'Table  Name'),
            'COLUMN_NAME' => Yii::t('app', 'Column  Name'),
            'REFERENCING_NAMES' => Yii::t('app', 'Referencing  Names'),
            'WHEN_CLAUSE' => Yii::t('app', 'When  Clause'),
            'STATUS' => Yii::t('app', 'Status'),
            'DESCRIPTION' => Yii::t('app', 'Description'),
            'ACTION_TYPE' => Yii::t('app', 'Action  Type'),
            'TRIGGER_BODY' => Yii::t('app', 'Trigger  Body'),
            'CROSSEDITION' => Yii::t('app', 'Crossedition'),
            'BEFORE_STATEMENT' => Yii::t('app', 'Before  Statement'),
            'BEFORE_ROW' => Yii::t('app', 'Before  Row'),
            'AFTER_ROW' => Yii::t('app', 'After  Row'),
            'AFTER_STATEMENT' => Yii::t('app', 'After  Statement'),
            'INSTEAD_OF_ROW' => Yii::t('app', 'Instead  Of  Row'),
            'FIRE_ONCE' => Yii::t('app', 'Fire  Once'),
            'APPLY_SERVER_ONLY' => Yii::t('app', 'Apply  Server  Only'),
        ];
    }
}
