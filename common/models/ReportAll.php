<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "REPORT_ALL".
 *
 * @property string $ID
 * @property string $YEAR
 * @property string $QUARTER
 * @property string $COUNT_IN
 * @property string $PRICE_IN
 * @property string $COUNT_OUT
 * @property string $PRICE_OUT
 * @property string $COUNT_CURRENT
 * @property string $PRICE_CURRENT
 */
class ReportAll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

//    public function behaviors() {
//        return [
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'CREATED_AT',
//                'updatedAtAttribute' => 'UPDATED_AT',
//                'value' => date('d-M-y h.i.s a'),
//            ],
//            [
//                'class' => BlameableBehavior::className(),
//                'createdByAttribute' => 'CREATED_BY',
//                'updatedByAttribute' => 'UPDATED_BY',
//                'value' => Yii::$app->user->id,
//            ],
//        ];
//    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'REPORT_ALL';
    }
    public static function primaryKey()
    {
        return ['ID'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'YEAR', 'COUNT_IN', 'PRICE_IN', 'COUNT_OUT', 'PRICE_OUT', 'COUNT_CURRENT', 'PRICE_CURRENT'], 'number'],
            [['QUARTER'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'YEAR' => Yii::t('app', 'Tahun'),
            'QUARTER' => Yii::t('app', 'Suku'),
            'COUNT_IN' => Yii::t('app', 'Bilangan Masuk'),
            'PRICE_IN' => Yii::t('app', 'Jumlah Masuk'),
            'COUNT_OUT' => Yii::t('app', 'Bilangan Keluar'),
            'PRICE_OUT' => Yii::t('app', 'Jumlah Keluar'),
            'COUNT_CURRENT' => Yii::t('app', 'Bilangan Semasa'),
            'PRICE_CURRENT' => Yii::t('app', 'Jumlah Semasa'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReportAllQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReportAllQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
//    public function getCreator() {
//        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
//    }
//    public function getUpdator() {
//        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
//    }
}
