<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "TRANSACTIONS".
 *
 * @property string $ID
 * @property string $TYPE
 * @property string $CHECK_DATE
 * @property integer $CHECK_BY
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TRANSACTIONS';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'TYPE'], 'number'],
            [['CHECK_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CHECK_DATE'], 'string'],
            [['ID'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TYPE' => Yii::t('app', 'Jenis'),
            'CHECK_DATE' => Yii::t('app', 'Tarikh Cek'),
            'CHECK_BY' => Yii::t('app', 'Cek Oleh'),
            'CREATED_AT' => Yii::t('app', 'Tarikh Dijana'),
            'UPDATED_AT' => Yii::t('app', 'Tarikh Dikemaskini'),
            'CREATED_BY' => Yii::t('app', 'Dijana Oleh'),
            'UPDATED_BY' => Yii::t('app', 'Dikemaskini Oleh'),
            'DELETED' => Yii::t('app', 'Status'),
            'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['ID' => 'INVENTORY_ID'])
                ->via('inventoriesCheckIn');
    }
    public function getInInventoryItem() {
        return $this->hasMany(InventoryItems::className(), ['ID' => 'CHECKIN_TRANSACTION_ID']);
    }
    public function getOutInventoryItem() {
        return $this->hasMany(InventoryItems::className(), ['ID' => 'CHECKOUT_TRANSACTION_ID']);
    }
    public function getOrder() {
        return $this->hasOne(Orders::className(), ['TRANSACTION_ID' => 'ID']);
    }
    public function getOrderItems() {
        return $this->hasOne(OrderItems::className(), ['ORDER_ID' => 'ID'])
                ->via('order');
    }
    public function getInventoriesCheckIn() {
        return $this->hasOne(InventoriesCheckIn::className(), ['TRANSACTION_ID' => 'ID']);
    }
    public function getCheckBy() {
        return $this->hasOne(People::className(), ['ID' => 'CHECK_BY']);
    }
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
