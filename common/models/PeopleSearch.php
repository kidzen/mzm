<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\People;

/**
 * PeopleSearch represents the model behind the search form about `common\models\People`.
 */
class PeopleSearch extends People {

    /**
     * @inheritdoc
     */
    public function attributes() {
        return array_merge(parent::attributes(), [
            'mpspProfile.NAMA',
            'mpspProfile.JAWATAN',
            'mpspProfile.KETERANGAN',
            'roles.NAME',
                ]
        );
    }

    public function rules() {
        return [
            [['ID'], 'number'],
            [['USERNAME', 'PROFILE_PIC', 'EMAIL', 'PASSWORD', 'DELETED_AT', 'CREATED_AT', 'UPDATED_AT', 'PASSWORD_RESET_TOKEN', 'AUTH_KEY',
            'mpspProfile.NAMA', 'mpspProfile.JAWATAN', 'mpspProfile.KETERANGAN', 'roles.NAME'], 'safe'],
            [['ROLE_ID', 'DELETED'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = People::find();
        $query->joinWith('mpspProfile');
        $query->joinWith('roles');
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['PEOPLE.DELETED' => 0]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->sort->attributes['ID'] = ['asc' => ['PEOPLE.ID' => SORT_ASC], 'desc' => ['PEOPLE.ID' => SORT_DESC],];
        $dataProvider->sort->attributes['DELETED_AT'] = ['asc' => ['PEOPLE.DELETED_AT' => SORT_ASC], 'desc' => ['PEOPLE.DELETED_AT' => SORT_DESC],];
        $dataProvider->sort->attributes['DELETED'] = ['asc' => ['PEOPLE.DELETED' => SORT_ASC], 'desc' => ['PEOPLE.DELETED' => SORT_DESC],];
        $dataProvider->sort->attributes['mpspProfile.NAMA'] = ['asc' => ['MPSP_STAFF.NAMA' => SORT_ASC], 'desc' => ['MPSP_STAFF.NAMA' => SORT_DESC],];
        $dataProvider->sort->attributes['mpspProfile.JAWATAN'] = ['asc' => ['MPSP_STAFF.JAWATAN' => SORT_ASC], 'desc' => ['MPSP_STAFF.JAWATAN' => SORT_DESC],];
        $dataProvider->sort->attributes['mpspProfile.KETERANGAN'] = ['asc' => ['MPSP_STAFF.KETERANGAN' => SORT_ASC], 'desc' => ['MPSP_STAFF.KETERANGAN' => SORT_DESC],];
        $dataProvider->sort->attributes['roles.NAME'] = ['asc' => ['ROLES.NAME' => SORT_ASC], 'desc' => ['ROLES.NAME' => SORT_DESC],];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PEOPLE.ID' => $this->ID,
            'PEOPLE.ROLE_ID' => $this->ROLE_ID,
            'ROLES.NAME' => $this->getAttribute('roles.NAME'),
            'PEOPLE.DELETED' => $this->DELETED,
//            'DELETED_AT' => $this->DELETED_AT,
//            'CREATED_AT' => $this->CREATED_AT,
//            'UPDATED_AT' => $this->UPDATED_AT,
        ]);

        $query->andFilterWhere(['like', 'USERNAME', $this->USERNAME])
                ->andFilterWhere(['like', 'EMAIL', $this->EMAIL])
                ->andFilterWhere(['like', 'MPSP_STAFF.NAMA', $this->getAttribute('mpspProfile.NAMA')])
                ->andFilterWhere(['like', 'MPSP_STAFF.JAWATAN', $this->getAttribute('mpspProfile.JAWATAN')])
                ->andFilterWhere(['like', 'MPSP_STAFF.KETERANGAN', $this->getAttribute('mpspProfile.KETERANGAN')])
                ->andFilterWhere(['like', 'PASSWORD', $this->PASSWORD])
                ->andFilterWhere(['like', 'PASSWORD_RESET_TOKEN', $this->PASSWORD_RESET_TOKEN])
                ->andFilterWhere(['like', 'PEOPLE.DELETED_AT', $this->DELETED_AT])
                ->andFilterWhere(['like', 'PEOPLE.CREATED_AT', $this->CREATED_AT])
                ->andFilterWhere(['like', 'PEOPLE.UPDATED_AT', $this->UPDATED_AT])
                ->andFilterWhere(['like', 'PEOPLE.AUTH_KEY', $this->AUTH_KEY]);

        return $dataProvider;
    }

}
