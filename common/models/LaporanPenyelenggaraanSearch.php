<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LaporanPenyelengaraan;

/**
 * LaporanPenyelenggaraanSearch represents the model behind the search form about `common\models\LaporanPenyelengaraan`.
 */
class LaporanPenyelenggaraanSearch extends LaporanPenyelengaraan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'number'],
            [['TAHUN', 'BULAN', 'NO_KERJA', 'ARAHAN_KERJA', 'JK_JG', 'NAMA_PANEL', 'KATEGORI_PANEL', 'TEMPOH_SELENGGARA', 'ALASAN', 'TARIKH_ARAHAN', 'NO_SEBUTHARGA', 'TARIKH_KENDERAAN_TIBA', 'TARIKH_JANGKA_SIAP', 'TARIKH_SEBUTHARGA', 'STATUS_SEBUTHARGA', 'NO_DO', 'TARIKH_SIAP', 'TEMPOH_JAMINAN', 'TARIKH_DO', 'TARIKH_CETAK', 'STATUS', 'CATATAN_SEBUTHARGA', 'CATATAN', 'NAMA_PIC', 'TARIKH_PERMOHONAN', 'KATEGORI_KEROSAKAN_ID', 'KATEGORI_KEROSAKAN', 'KATEGORI', 'ISSERVICE', 'ISPANCIT', 'ODOMETER_TERKINI', 'NO_PLAT', 'MODEL', 'KOD_JABATAN', 'NAMA_JABATAN', 'JUMLAH_KOS'], 'safe'],
            [['BENGKEL_PANEL_ID', 'SELENGGARA_ID', 'ALASAN_ID', 'ID_KENDERAAN'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LaporanPenyelengaraan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'BENGKEL_PANEL_ID' => $this->BENGKEL_PANEL_ID,
            'SELENGGARA_ID' => $this->SELENGGARA_ID,
            'ALASAN_ID' => $this->ALASAN_ID,
            'ID_KENDERAAN' => $this->ID_KENDERAAN,
        ]);

        $query->andFilterWhere(['like', 'TAHUN', $this->TAHUN])
            ->andFilterWhere(['like', 'BULAN', $this->BULAN])
            ->andFilterWhere(['like', 'NO_KERJA', $this->NO_KERJA])
            ->andFilterWhere(['like', 'ARAHAN_KERJA', $this->ARAHAN_KERJA])
            ->andFilterWhere(['like', 'JK_JG', $this->JK_JG])
            ->andFilterWhere(['like', 'NAMA_PANEL', $this->NAMA_PANEL])
            ->andFilterWhere(['like', 'KATEGORI_PANEL', $this->KATEGORI_PANEL])
            ->andFilterWhere(['like', 'TEMPOH_SELENGGARA', $this->TEMPOH_SELENGGARA])
            ->andFilterWhere(['like', 'ALASAN', $this->ALASAN])
            ->andFilterWhere(['like', 'TARIKH_ARAHAN', $this->TARIKH_ARAHAN])
            ->andFilterWhere(['like', 'NO_SEBUTHARGA', $this->NO_SEBUTHARGA])
            ->andFilterWhere(['like', 'TARIKH_KENDERAAN_TIBA', $this->TARIKH_KENDERAAN_TIBA])
            ->andFilterWhere(['like', 'TARIKH_JANGKA_SIAP', $this->TARIKH_JANGKA_SIAP])
            ->andFilterWhere(['like', 'TARIKH_SEBUTHARGA', $this->TARIKH_SEBUTHARGA])
            ->andFilterWhere(['like', 'STATUS_SEBUTHARGA', $this->STATUS_SEBUTHARGA])
            ->andFilterWhere(['like', 'NO_DO', $this->NO_DO])
            ->andFilterWhere(['like', 'TARIKH_SIAP', $this->TARIKH_SIAP])
            ->andFilterWhere(['like', 'TEMPOH_JAMINAN', $this->TEMPOH_JAMINAN])
            ->andFilterWhere(['like', 'TARIKH_DO', $this->TARIKH_DO])
            ->andFilterWhere(['like', 'TARIKH_CETAK', $this->TARIKH_CETAK])
            ->andFilterWhere(['like', 'STATUS', $this->STATUS])
            ->andFilterWhere(['like', 'CATATAN_SEBUTHARGA', $this->CATATAN_SEBUTHARGA])
            ->andFilterWhere(['like', 'CATATAN', $this->CATATAN])
            ->andFilterWhere(['like', 'NAMA_PIC', $this->NAMA_PIC])
            ->andFilterWhere(['like', 'TARIKH_PERMOHONAN', $this->TARIKH_PERMOHONAN])
            ->andFilterWhere(['like', 'KATEGORI_KEROSAKAN_ID', $this->KATEGORI_KEROSAKAN_ID])
            ->andFilterWhere(['like', 'KATEGORI_KEROSAKAN', $this->KATEGORI_KEROSAKAN])
            ->andFilterWhere(['like', 'KATEGORI', $this->KATEGORI])
            ->andFilterWhere(['like', 'ISSERVICE', $this->ISSERVICE])
            ->andFilterWhere(['like', 'ISPANCIT', $this->ISPANCIT])
            ->andFilterWhere(['like', 'ODOMETER_TERKINI', $this->ODOMETER_TERKINI])
            ->andFilterWhere(['like', 'NO_PLAT', $this->NO_PLAT])
            ->andFilterWhere(['like', 'MODEL', $this->MODEL])
            ->andFilterWhere(['like', 'KOD_JABATAN', $this->KOD_JABATAN])
            ->andFilterWhere(['like', 'NAMA_JABATAN', $this->NAMA_JABATAN])
            ->andFilterWhere(['like', 'JUMLAH_KOS', $this->JUMLAH_KOS]);

        return $dataProvider;
    }
}
