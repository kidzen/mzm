<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "KEWPS_10".
 *
 * @property string $ORDER_ID
 * @property string $ORDER_REQUIRED_DATE
 * @property string $ORDER_ITEM_ID
 * @property string $ORDER_NO
 * @property string $ORDERED_BY
 * @property string $ORDER_DATE
 * @property string $TRANSACTION_ID
 * @property string $CARD_NO
 * @property string $CODE_NO
 * @property string $DESCRIPTION
 * @property string $RQ_QUANTITY
 * @property string $APP_QUANTITY
 * @property string $CURRENT_BALANCE
 * @property string $UNIT_PRICE
 * @property string $AVERAGE_UNIT_PRICE
 * @property string $BATCH_TOTAL_PRICE
 * @property string $CREATED_DATE
 * @property integer $CREATED_BY
 * @property integer $APPROVED_BY
 * @property string $APPROVED_DATE
 */
class FormKewps10 extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'KEWPS_10';
    }

    public static function primaryKey() {
        return ['ORDER_ITEM_ID'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ORDER_ID', 'ORDER_ITEM_ID', 'TRANSACTION_ID', 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'UNIT_PRICE', 'AVERAGE_UNIT_PRICE', 'BATCH_TOTAL_PRICE'], 'number'],
            [['CREATED_BY', 'APPROVED_BY'], 'integer'],
            [['ORDER_REQUIRED_DATE', 'ORDER_DATE'], 'string', 'max' => 7],
            [['ORDER_NO', 'ORDERED_BY'], 'string', 'max' => 20],
            [['CARD_NO', 'CODE_NO', 'DESCRIPTION'], 'string', 'max' => 255],
            [['CREATED_DATE', 'APPROVED_DATE'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'ORDER_ID' => Yii::t('app', 'Order  ID'),
            'ORDER_REQUIRED_DATE' => Yii::t('app', 'Order  Required  Date'),
            'ORDER_ITEM_ID' => Yii::t('app', 'Order  Item  ID'),
            'ORDER_NO' => Yii::t('app', 'Order  No'),
            'ORDERED_BY' => Yii::t('app', 'Ordered  By'),
            'ORDER_DATE' => Yii::t('app', 'Order  Date'),
            'TRANSACTION_ID' => Yii::t('app', 'Transaction  ID'),
            'CARD_NO' => Yii::t('app', 'Card  No'),
            'CODE_NO' => Yii::t('app', 'Code  No'),
            'DESCRIPTION' => Yii::t('app', 'Description'),
            'RQ_QUANTITY' => Yii::t('app', 'Rq  Quantity'),
            'APP_QUANTITY' => Yii::t('app', 'App  Quantity'),
            'CURRENT_BALANCE' => Yii::t('app', 'Current  Balance'),
            'UNIT_PRICE' => Yii::t('app', 'Unit  Price'),
            'AVERAGE_UNIT_PRICE' => Yii::t('app', 'Average  Unit  Price'),
            'BATCH_TOTAL_PRICE' => Yii::t('app', 'Batch  Total  Price'),
            'CREATED_DATE' => Yii::t('app', 'Created  Date'),
            'CREATED_BY' => Yii::t('app', 'Created  By'),
            'APPROVED_BY' => Yii::t('app', 'Approved  By'),
            'APPROVED_DATE' => Yii::t('app', 'Approved  Date'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getApprovedBy() {
        return $this->hasOne(MpspStaff::className(), ['NO_PEKERJA' => 'STAFF_NO'])
                        ->viaTable('{{%PEOPLE}}', ['ID' => 'APPROVED_BY']);
    }

    public function getCreatedBy() {
        return $this->hasOne(MpspStaff::className(), ['NO_PEKERJA' => 'STAFF_NO'])
                        ->viaTable('{{%PEOPLE}}', ['ID' => 'CREATED_BY']);
    }


    public function getRecordBy() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }

}
