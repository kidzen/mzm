<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "TRANSACTIONS_ALL".
 *
 * @property string $ID
 * @property string $TYPE
 * @property string $CHECK_DATE
 * @property integer $INVENTORY_ID
 * @property string $COUNT_IN
 * @property string $PRICE_IN
 * @property string $COUNT_OUT
 * @property string $PRICE_OUT
 * @property string $COUNT_CURRENT
 * @property string $PRICE_CURRENT
 */
class TransactionsAll extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TRANSACTIONS_ALL';
    }
    public static function primaryKey() {
        return ['ID'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ID', 'TYPE', 'UNIT_PRICE', 'COUNT_IN', 'PRICE_IN', 'COUNT_OUT', 'PRICE_OUT', 'COUNT_CURRENT', 'PRICE_CURRENT'], 'number'],
            [['INVENTORY_ID', 'CHECK_BY'], 'integer'],
            [['CHECK_DATE', 'REFFERENCE'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TYPE' => Yii::t('app', 'Jenis'),
            'CHECK_DATE' => Yii::t('app', 'Tarikh Cek'),
            'CHECK_BY' => Yii::t('app', 'Cek Oleh'),
            'REFFERENCE' => Yii::t('app', 'Tarikh Cek'),
            'INVENTORY_ID' => Yii::t('app', 'ID Inventori'),
            'UNIT_PRICE' => Yii::t('app', 'Harga Seunit (RM)'),
            'COUNT_IN' => Yii::t('app', 'Bil Kemasukan'),
            'PRICE_IN' => Yii::t('app', 'Jumlah Kemasukan'),
            'COUNT_OUT' => Yii::t('app', 'Bil Keluar'),
            'PRICE_OUT' => Yii::t('app', 'Jumlah Keluar'),
            'COUNT_CURRENT' => Yii::t('app', 'Bil Semasa'),
            'PRICE_CURRENT' => Yii::t('app', 'Jumlah Semasa'),
        ];
    }

    /**
     * @inheritdoc
     * @return TransactionsAllQuery the active query used by this AR class.
     */
    public static function find() {
        return new TransactionsAllQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function getTransaction() {
        return $this->hasOne(Transactions::className(), ['ID' => 'ID']);
    }

    public function getInout() {
        if ($this->TYPE == 2) {
            return $this->hasOne(Orders::className(), ['TRANSACTION_ID' => 'ID'])
                            ->via('transaction');
        } else if ($this->TYPE == 1) {
            return $this->hasOne(InventoriesCheckIn::className(), ['TRANSACTION_ID' => 'ID'])
                            ->via('transaction');
        }
    }

    public function getOrder() {
        if ($this->TYPE == 2) {
            return $this->hasOne(Orders::className(), ['TRANSACTION_ID' => 'ID'])
                            ->via('transaction');
        } else if ($this->TYPE == 1) {
            return 'IN';
        }
    }

    public function getVehicle() {
        if ($this->TYPE == 2) {
            return $this->hasOne(VehicleList::className(), ['ID' => 'VEHICLE_ID'])
                            ->via('order');
        } else if ($this->TYPE == 1) {
            return 'IN';
        }
    }

    public function getCheckBy() {
        return $this->hasOne(People::className(), ['ID' => 'CHECK_BY']);
    }
    
    
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }

}
