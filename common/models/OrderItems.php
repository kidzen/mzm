<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "ORDER_ITEMS".
 *
 * @property string $ID
 * @property integer $INVENTORY_ID
 * @property integer $ORDER_ID
 * @property string $RQ_QUANTITY
 * @property string $APP_QUANTITY
 * @property string $CURRENT_BALANCE
 * @property string $UNIT_PRICE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class OrderItems extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
        [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'CREATED_AT',
        'updatedAtAttribute' => 'UPDATED_AT',
        'value' => date('d-M-y h.i.s a'),
        ],
        [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'CREATED_BY',
        'updatedByAttribute' => 'UPDATED_BY',
        'value' => Yii::$app->user->id,
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ORDER_ITEMS';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        [['RQ_QUANTITY', 'INVENTORY_ID'], 'required', 'on' => 'checkout'],
        [['ID', 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'UNIT_PRICE'], 'number'],
        [['INVENTORY_ID', 'ORDER_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
        [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        [['ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        'ID' => Yii::t('app', 'ID'),
        'INVENTORY_ID' => Yii::t('app', 'ID Inventori'),
        'ORDER_ID' => Yii::t('app', 'ID Pesanan'),
        'RQ_QUANTITY' => Yii::t('app', 'Kuantiti Dimohon'),
        'APP_QUANTITY' => Yii::t('app', 'Kuantiti Disahkan'),
        'CURRENT_BALANCE' => Yii::t('app', 'Kuantiti Semasa Pengeluaran'),
        'UNIT_PRICE' => Yii::t('app', 'Harga Seunit'),
        'CREATED_AT' => Yii::t('app', 'Tarikh Dijana'),
        'UPDATED_AT' => Yii::t('app', 'Tarikh Dikemaskini'),
        'CREATED_BY' => Yii::t('app', 'Dijana Oleh'),
        'UPDATED_BY' => Yii::t('app', 'Dikemaskini Oleh'),
        'DELETED' => Yii::t('app', 'Status'),
        'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['ID' => 'INVENTORY_ID']);
    }

    public function getVehicle() {
        return $this->hasOne(VehicleList::className(), ['ID' => 'VEHICLE_ID'])
        ->via('order');
    }

    public function getOrder() {
        return $this->hasOne(Orders::className(), ['ID' => 'ORDER_ID']);
    }

    public function getOrderApprovedBy() {
        return $this->hasOne(People::className(), ['ID' => 'APPROVED_BY'])
        ->via('order');
    }

    public function getOrderCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY'])
        ->via('order');
    }


    public function getArahanKerja() {
        return $this->hasOne(LaporanPenyelengaraanSisken::className(), ['ID' => 'ARAHAN_KERJA_ID'])
        ->via('order');
    }

    public function getTransaction() {
        return $this->hasOne(Transactions::className(), ['ID' => 'TRANSACTION_ID'])
        ->via('order');
    }

    public function getItems() {
        return $this->hasMany(InventoryItems::className(), ['CHECKOUT_TRANSACTION_ID' => 'ID']);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }

    public static function findAllByOrderItemId($id) {
        return static::find(['ORDERS.ID' => static::findOne($id)]);
    }

    public static function updateAppQuantity($id) {
        $itemCount = \common\models\InventoryItems::find()->where(['CHECKOUT_TRANSACTION_ID' => $id])->count();
        $modelOrderItem = static::findOne($id);
        $modelOrderItem->APP_QUANTITY = $itemCount;
        if ($modelOrderItem->save(false)) {
            return true;
        }
        return false;
    }

    public static function checkAppQuantity() {
        $unsync = \common\models\OrderItemsAppQuantityCheck::find()->count();
        if ($unsync === 0) {
            return true;
        }
        return false;
    }

    public static function getFaultyAppQuantity() {
        $ids = \common\models\OrderItemsAppQuantityCheck::find()->all();
        return $ids->ID;
    }

}
