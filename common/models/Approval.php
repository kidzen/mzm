<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "APPROVAL".
 *
 * @property string $ID
 * @property string $ID_INVENTORY_ITEMS
 * @property string $ID_INVENTORIES
 * @property string $ID_CATEGORIES
 * @property string $ID_INVENTORIES_CHECKIN
 * @property string $ID_ORDER_ITEMS
 * @property string $ID_ORDERS
 * @property string $SKU
 * @property string $ODER
 * @property string $ORDER_NO
 * @property string $ITEMS_CATEGORY_NAME
 * @property string $ITEMS_INVENTORY_NAME
 * @property string $RQ_QUANTITY
 * @property string $APP_QUANTITY
 * @property integer $APPROVED
 * @property string $CURRENT_BALANCE
 * @property string $TOTAL_PRICE
 * @property string $ORDER_DATE
 * @property string $REQUIRED_DATE
 */
class Approval extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'APPROVAL';
    }

    public static function primaryKey() {
        return ['ID'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
//            [['ID', 'ID_INVENTORY_ITEMS', 'ID_INVENTORIES', 'ID_CATEGORIES', 'ID_INVENTORIES_CHECKIN', 'ID_ORDER_ITEMS', 'ID_ORDERS'
//                , 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'TOTAL_PRICE', 'UNIT_PRICE'], 'number'],
            
            [['ID', 'AVAILABLE_PER_ORDER', 'ID_INVENTORY_ITEMS', 'ID_INVENTORIES', 'ID_CATEGORIES', 'ID_INVENTORIES_CHECKIN', 
                'ID_ORDER_ITEMS', 'ID_ORDERS', 'UNIT_PRICE', 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'TOTAL_PRICE'], 'number'],
            
            [['ID_INVENTORY_ITEMS'], 'required'],
            [['APPROVED'], 'integer'],
            [['SKU'], 'string', 'max' => 50],
            [['ORDER_NO'], 'string', 'max' => 20],
            [['ITEMS_CATEGORY_NAME', 'ITEMS_INVENTORY_NAME'], 'string', 'max' => 255],
            [['ORDER_DATE', 'REQUIRED_DATE'], 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'ID' => Yii::t('app', 'ID'),
            'AVAILABLE_PER_ORDER' => Yii::t('app', 'Jumlah Yang Ada'), 
            'ID_INVENTORY_ITEMS' => Yii::t('app', 'Id  Barang Inventori'),
            'ID_INVENTORIES' => Yii::t('app', 'Id  Inventori'),
            'ID_CATEGORIES' => Yii::t('app', 'Id  Kategori'),
            'ID_INVENTORIES_CHECKIN' => Yii::t('app', 'Id  Inventori Masuk'),
            'ID_ORDER_ITEMS' => Yii::t('app', 'Id  Barang Pesanan'),
            'ID_ORDERS' => Yii::t('app', 'Id  Pesanan'),
            'SKU' => Yii::t('app', 'Sku'),
            'UNIT_PRICE' => Yii::t('app', 'Harga Seunit'),
            'ORDER_NO' => Yii::t('app', 'No Pesanan'),
            'ITEMS_CATEGORY_NAME' => Yii::t('app', 'Nama Kategori Barang'),
            'ITEMS_INVENTORY_NAME' => Yii::t('app', 'Nama Inventori Barang'),
            'RQ_QUANTITY' => Yii::t('app', 'Kuantiti Dimohon'),
            'APP_QUANTITY' => Yii::t('app', 'Kuantiti Disahkan'),
            'APPROVED' => Yii::t('app', 'Status Pengesahan'),
            'CURRENT_BALANCE' => Yii::t('app', 'Kuantiti Semasa Pengeluaran'),
            'TOTAL_PRICE' => Yii::t('app', 'Jumlah'),
            'ORDER_DATE' => Yii::t('app', 'Tarikh Pesanan'),
            'REQUIRED_DATE' => Yii::t('app', 'Tarikh Diperlukan'),
        ];
    }

    /**
     * @inheritdoc
     * @return ApprovalQuery the active query used by this AR class.
     */
    public static function find() {
        return new ApprovalQuery(get_called_class());
    }

    public static function fifo($orderId) {
        $data = static::find()->select('ID_INVENTORIES')->where(['ID_ORDERS'=>$orderId])->groupBy('ID_INVENTORIES')
                ->asArray()
                ->all();
        $itemlist = InventoryItems::find()
                ->joinWith('inventory')
                ->where(['in','INVENTORIES.ID',$data[0]])
                ->andWhere(['CHECKOUT_TRANSACTION_ID'=>null])
//                ->asArray()
                ->all();
        $up = InventoryItems::checkoutFifo($data[0]);
//        for
        var_dump($up);die();
        return $itemlist;
//        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['ID' => 'ID_INVENTORIES']);
    }

    public function getInventoryItem() {
        return $this->hasOne(InventoryItems::className(), ['ID' => 'ID_INVENTORY_ITEMS']);
    }

    public function getOrder() {
        return $this->hasOne(Orders::className(), ['ID' => 'ID_ORDERS']);
    }

    public function getOrderItem() {
        return $this->hasOne(OrderItems::className(), ['ID' => 'ID_ORDER_ITEMS']);
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['ID' => 'ID_CATEGORIES']);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }

}
