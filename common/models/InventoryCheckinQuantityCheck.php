<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "IC_QUANTITY_CHECK".
 *
 * @property string $IV_ID
 * @property string $IC_ID
 * @property string $IV_QUANTITY
 * @property string $ACTUAL_QUANTITY
 */
class InventoryCheckinQuantityCheck extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'IC_QUANTITY_CHECK';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IV_ID', 'IC_ID', 'IV_QUANTITY', 'ACTUAL_QUANTITY'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IV_ID' => Yii::t('app', 'Iv  ID'),
            'IC_ID' => Yii::t('app', 'Ic  ID'),
            'IV_QUANTITY' => Yii::t('app', 'Iv  Quantity'),
            'ACTUAL_QUANTITY' => Yii::t('app', 'Actual  Quantity'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
