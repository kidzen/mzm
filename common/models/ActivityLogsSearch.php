<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ActivityLogs;

/**
 * ActivityLogsSearch represents the model behind the search form about `common\models\ActivityLogs`.
 */
class ActivityLogsSearch extends ActivityLogs {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ID'], 'number'],
            [['USER_ID'], 'integer'],
            [['REMOTE_IP', 'ACTION', 'CONTROLLER', 'PARAMS', 'ROUTE', 'STATUS', 'MESSAGES', 'CREATED_AT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ActivityLogs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['CREATED_AT' => SORT_DESC]],
        ]);

        $this->load($params);
        
//        if (!($this->load($params) && $this->validate())) {
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'USER_ID' => $this->USER_ID,
//            'CREATED_AT' => $this->CREATED_AT,
        ]);

        $query->andFilterWhere(['like', 'REMOTE_IP', $this->REMOTE_IP])
                ->andFilterWhere(['like', 'ACTION', $this->ACTION])
                ->andFilterWhere(['like', 'CONTROLLER', $this->CONTROLLER])
                ->andFilterWhere(['like', 'PARAMS', $this->PARAMS])
                ->andFilterWhere(['like', 'ROUTE', $this->ROUTE])
                ->andFilterWhere(['like', 'STATUS', $this->STATUS])
                ->andFilterWhere(['like', 'CREATED_AT', $this->CREATED_AT])
                ->andFilterWhere(['like', 'MESSAGES', $this->MESSAGES]);

        return $dataProvider;
    }

}
