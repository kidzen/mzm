<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ReportAll]].
 *
 * @see ReportAll
 */
class ReportAllQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ReportAll[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ReportAll|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
