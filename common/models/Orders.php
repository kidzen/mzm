<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "ORDERS".
 *
 * @property string $ID
 * @property integer $TRANSACTION_ID
 * @property string $ORDER_DATE
 * @property integer $ORDERED_BY
 * @property string $ORDER_NO
 * @property integer $APPROVED
 * @property integer $APPROVED_BY
 * @property string $APPROVED_AT
 * @property integer $VEHICLE_ID
 * @property string $REQUIRED_DATE
 * @property string $CHECKOUT_DATE
 * @property integer $CHECKOUT_BY
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class Orders extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
        [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'CREATED_AT',
        'updatedAtAttribute' => 'UPDATED_AT',
        'value' => date('d-M-y h.i.s a'),
        ],
        [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'CREATED_BY',
        'updatedByAttribute' => 'UPDATED_BY',
        'value' => Yii::$app->user->id,
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ORDERS';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        [['ID'], 'number'],
        [['ORDERED_BY', 'ORDER_NO', 'ORDER_DATE', 'REQUIRED_DATE',], 'required', 'on' => 'checkout'],
        [['TRANSACTION_ID', 'APPROVED', 'APPROVED_BY', 'VEHICLE_ID', 'CHECKOUT_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
        [['APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
//            [['ORDER_DATE', 'REQUIRED_DATE', 'CHECKOUT_DATE'], 'string', 'max' => 7],
        [['ORDER_DATE', 'REQUIRED_DATE', 'CHECKOUT_DATE', 'ARAHAN_KERJA_ID'], 'string'],
        ['REQUIRED_DATE', 'compare', 'compareAttribute' => 'ORDER_DATE', 'operator' => '>=',
        'enableClientValidation' => true, 'message' => '"Tarikh Diperlukan" tidak boleh mendahului "Tarikh Pesanan"'],
        [['ORDERED_BY', 'ORDER_NO'], 'string', 'max' => 20],
        [['ID'], 'unique'],
        ];
    }

//    public function scenarios() {
//        return [
//            'default' => ['ORDERED_BY', 'ORDER_NO', 'ORDER_DATE', 'REQUIRED_DATE',],
//            'checkout' => ['ORDERED_BY', 'ORDER_NO', 'ORDER_DATE', 'REQUIRED_DATE'],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        'ID' => Yii::t('app', 'ID'),
        'ARAHAN_KERJA_ID' => Yii::t('app', 'No Arahan Kerja'),
        'TRANSACTION_ID' => Yii::t('app', 'Transaction  ID'),
        'ORDER_DATE' => Yii::t('app', 'Tarikh Pesanan'),
        'ORDERED_BY' => Yii::t('app', 'Dipesan Oleh'),
        'ORDER_NO' => Yii::t('app', 'No Pesanan'),
        'APPROVED' => Yii::t('app', 'Status Pengesahan'),
        'APPROVED_BY' => Yii::t('app', 'Disahkan Oleh'),
        'APPROVED_AT' => Yii::t('app', 'Disahkan Pada'),
        'VEHICLE_ID' => Yii::t('app', 'ID Kenderaan'),
        'REQUIRED_DATE' => Yii::t('app', 'Tarikh Diperlukan'),
        'CHECKOUT_DATE' => Yii::t('app', 'Tarikh Keluar'),
        'CHECKOUT_BY' => Yii::t('app', 'Dikeluarkan Oleh'),
        'CREATED_AT' => Yii::t('app', 'Tarikh Dijana'),
        'UPDATED_AT' => Yii::t('app', 'Tarikh Dikemaskini'),
        'CREATED_BY' => Yii::t('app', 'Dijana Oleh'),
        'UPDATED_BY' => Yii::t('app', 'Dikemaskini Oleh'),
        'DELETED' => Yii::t('app', 'Status'),
        'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getItems() {
        return $this->hasMany(OrderItems::className(), ['ORDER_ID' => 'ID']);
    }
    public function getArahanKerja() {
        return $this->hasOne(LaporanPenyelengaraanSisken::className(), ['ID' => 'ARAHAN_KERJA_ID']);
    }

    public function getVehicle() {
        return $this->hasOne(VehicleList::className(), ['ID' => 'VEHICLE_ID']);
    }

    public function getApprovedBy() {
        return $this->hasOne(People::className(), ['ID' => 'APPROVED_BY']);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }

    public static function pdfKewps10($id) {
        static::findOne($id);
        $query = OrderItems::find()->with('inventory', 'order','orderApprovedBy')->where(['ORDER_ID' => $id]);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query->asArray()->all(),
            ]);
        $items = $dataProvider->models;
//        $par = array_chunk($items, 3);
//        var_dump($par);die();
        $content = \Yii::$app->controller->renderPartial('@frontend/views/request/_kewps102', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
//        $filename = '@frontend/assets/dist/pdf_template/KEW.PS-82.pdf';
//        $filedir = Yii::getAlias($filename);


        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
            'defaultFontSize' => 10,
//            'defaultFont' => '',
//
            'marginLeft' => 13,
            'marginRight' => 13,
            'marginTop' => 10,
            'marginBottom' => 6,
            'marginHeader' => 5,
            'marginFooter' => 5,
//            'marginLeft' => 15,
//            'marginRight' => 15,
//            'marginTop' => 16,
//            'marginBottom' => 16,
//            'marginHeader' => 9,
//            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
            'SetFooter' => ['|{PAGENO}|'],
            ]
            ]);
        $pdf->render();
    }

    public static function pdfKewps102($id) {
        // var_dump(strlen('BULB 24V 1141 (LAMPU SIGNAL)'));die();
        static::findOne($id);
        $query = OrderItems::find()->with('inventory','order.approvedBy.mpspProfile')->where(['ORDER_ID' => $id]);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query->asArray()->all(),
            ]);
        $items = $dataProvider->models;
        // var_dump($items[0]['order']);die();
//         var_dump($items);die();
        $arraylist = array_chunk($items, 5);

        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $contentHead = \Yii::$app->controller->renderPartial('@frontend/views/request/_kewps102_1', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        $contentBody = [];
        foreach ($arraylist as $list) {
            $contentBody[] = \Yii::$app->controller->renderPartial('@frontend/views/request/_kewps102_2', [
                'items' => $list,
                'dataProvider' => $dataProvider,
                ]);
        }
        $contentFoot = \Yii::$app->controller->renderPartial('@frontend/views/request/_kewps102_3', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        // $filename = '@frontend/assets/dist/pdf_template/KEW.PS-82.pdf';
        // $filedir = Yii::getAlias($filename);
        $mpdf = new \mPDF('utf-8','A4-L');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Muzzam Teknologi Sdn Bhd|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('|{PAGENO}|Dicetak pada '.strftime("%A, %d %B %Y %H:%M:%S"));
        $mpdf->WriteHTML($css, 1);
        // $mpdf->WriteHTML('.row-1{white-space:nowrap;} .colw-1{ width:1%;white-space:nowrap;} .colw-10{ width:10%;white-space:nowrap;} .colw-20{ width:20%;white-space:nowrap;} .colw-40{ width:30%;white-space:nowrap;} .colw-812{ width:8.12%;white-space:nowrap;}', 1);
//        var_dump($contentHead);
       // var_dump($contentBody);
       // var_dump($contentFoot);
       //  die();
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if($i != sizeof($contentBody)-1){
                $mpdf->AddPage();
            }
           // var_dump($body);
        }
        // die();
        $mpdf->Output();
        exit;
    }

}
