<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "ACTIVITY_LOGS".
 *
 * @property string $ID
 * @property integer $USER_ID
 * @property string $REMOTE_IP
 * @property string $ACTION
 * @property string $CONTROLLER
 * @property string $PARAMS
 * @property string $ROUTE
 * @property string $STATUS
 * @property string $MESSAGES
 * @property string $CREATED_AT
 */
class FormGenerator extends Model {

    const LOG_STATUS_SUCCESS = 'success';
    const LOG_STATUS_INFO = 'info';
    const LOG_STATUS_WARNING = 'warning';
    const LOG_STATUS_ERROR = 'error';
    const LOG_STATUS_FAIL = 'fail';

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public $ID;
    public $FORM_ID;
    public $FORM_NAME;
    public $PARAMS;
    public $INVENTORY_ID;
    public $INVENTORY_LIST;
    public $ORDER_ID;
    public $YEAR;
    public $MONTH;
    public $printAll;



    /**
     * @inheritdoc
     */
//    public static function tableName() {
//        return 'ACTIVITY_LOGS';
//    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['FORM_ID'], 'required'],
            [['FORM_ID','ID'], 'number'],
            [['INVENTORY_ID','INVENTORY_LIST','ORDER_ID','YEAR','MONTH','printAll'], 'integer'],
            [[ 'FORM_NAME', 'PARAMS'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'ID' => 'ID',
            'YEAR' => 'Tahun',
            'MONTH' => 'Bulan',
            'ORDER_ID' => 'No Pesanan',
            'INVENTORY_ID' => 'Nama Inventori',
            'INVENTORY_LIST' => 'Senarai inventori',
            'FORM_ID' => 'ID Borang',
            'FORM_NAME' => 'Nama Borang',
            'PARAMS' => 'Maklumat Borang',
            'printAll' => 'Cetak Semua Transaksi',
        ];
    }

    /**
     * Adds a message to ActionLog model
     *
     * @param string $status The log status information
     * @param mixed $message The log message
     * @param int $uID The user id
     */
//    public static function add($status = null, $message = null) {
//        $model = Yii::createObject(__CLASS__);
//        $model->USER_ID = (int)Yii::$app->user->id;
//        $model->REMOTE_IP = $_SERVER['REMOTE_ADDR'];
//        $model->ACTION = Yii::$app->requestedAction->id;
//        $model->CONTROLLER = Yii::$app->requestedAction->controller->id;
//        $model->PARAMS = serialize(Yii::$app->requestedAction->controller->actionParams);
//        $model->ROUTE = Yii::$app->requestedRoute;
//        $model->STATUS = $status;
//        $model->MESSAGES = ($message !== null) ? serialize($message) : null;
//
//        return $model->save();
//    }
}
