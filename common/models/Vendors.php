<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "VENDORS".
 *
 * @property string $ID
 * @property string $NAME
 * @property string $ADDRESS
 * @property string $CONTACT_NO
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class Vendors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'VENDORS';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['ID'], 'required'],
            [['ID'], 'number'],
            [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['NAME', 'ADDRESS'], 'string', 'max' => 255],
            [['CONTACT_NO'], 'string', 'max' => 50],
            [['ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'NAME' => Yii::t('app', 'Vendor'),
            'ADDRESS' => Yii::t('app', 'Alamat'),
            'CONTACT_NO' => Yii::t('app', 'No Tel'),
            'CREATED_AT' => Yii::t('app', 'Tarikh Dijana'),
            'UPDATED_AT' => Yii::t('app', 'Tarikh Dikemaskini'),
            'CREATED_BY' => Yii::t('app', 'Dijana Oleh'),
            'UPDATED_BY' => Yii::t('app', 'Dikemaskini oleh'),
            'DELETED' => Yii::t('app', 'Status'),
            'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
