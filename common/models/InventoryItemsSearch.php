<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InventoryItems;

/**
 * InventoryItemsSearch represents the model behind the search form about `common\models\InventoryItems`.
 */
class InventoryItemsSearch extends InventoryItems {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ID', 'UNIT_PRICE'], 'number'],
            [['INVENTORY_ID', 'CHECKIN_TRANSACTION_ID', 'CHECKOUT_TRANSACTION_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['SKU', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = InventoryItems::find();
        $query->joinWith('category');
//        $query->joinWith('transactionOut');
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['INVENTORY_ITEMS.DELETED' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = ['CREATED_AT' => SORT_DESC];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['ID'] = [
            'asc' => ['INVENTORY_ITEMS.ID' => SORT_ASC],
            'desc' => ['INVENTORY_ITEMS.ID' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['DELETED'] = [
            'asc' => ['INVENTORY_ITEMS.DELETED' => SORT_ASC],
            'desc' => ['INVENTORY_ITEMS.DELETED' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['inventory.DESCRIPTION'] = [
            'asc' => ['INVENTORIES.DESCRIPTION' => SORT_ASC],
            'desc' => ['INVENTORIES.DESCRIPTION' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['CREATED_AT'] = [
            'asc' => ['INVENTORY_ITEMS.CREATED_AT' => SORT_ASC],
            'desc' => ['INVENTORY_ITEMS.CREATED_AT' => SORT_DESC],
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'INVENTORY_ITEMS.ID' => $this->ID,
            'INVENTORY_ITEMS.INVENTORY_ID' => $this->INVENTORY_ID,
            'INVENTORY_ITEMS.CHECKIN_TRANSACTION_ID' => $this->CHECKIN_TRANSACTION_ID,
            'INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' => $this->CHECKOUT_TRANSACTION_ID,
            'INVENTORY_ITEMS.UNIT_PRICE' => $this->UNIT_PRICE,
            'INVENTORY_ITEMS.CREATED_AT' => $this->CREATED_AT,
            'INVENTORY_ITEMS.UPDATED_AT' => $this->UPDATED_AT,
            'INVENTORY_ITEMS.CREATED_BY' => $this->CREATED_BY,
            'INVENTORY_ITEMS.UPDATED_BY' => $this->UPDATED_BY,
            'INVENTORY_ITEMS.DELETED' => $this->DELETED,
            'INVENTORY_ITEMS.DELETED_AT' => $this->DELETED_AT,
        ]);

        $query->andFilterWhere(['like', 'INVENTORY_ITEMS.SKU', $this->SKU]);

        return $dataProvider;
    }

}
