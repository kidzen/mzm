<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;
use common\models\Orders;
use common\models\OrderItems;
use yii\data\ArrayDataProvider;

/**
 * This is the model class for table "ORDERS".
 *
 * @property string $ID
 * @property integer $TRANS_ID
 * @property string $ORDER_DATE
 * @property integer $ORDERED_BY
 * @property string $ORDER_NO
 * @property integer $APPROVED
 * @property integer $APPROVED_BY
 * @property string $APPROVED_AT
 * @property integer $VEHICLE_ID
 * @property string $REQUIRED_DATE
 * @property string $CHECKOUT_DATE
 * @property integer $CHECKOUT_BY
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class KewpsForm {

    public static function pdfKewps10($id) {
        Orders::findOne($id);
        $query = OrderItems::find()->with('inventory', 'order.approvedBy.mpspProfile')->where(['ORDER_ID' => $id]);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->asArray()->all(),
            ]);
        $items = $dataProvider->models;
        $arraylist = array_chunk($items, 5);

        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps102_1', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        $contentBody = [];
        foreach ($arraylist as $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps102_2', [
                'items' => $list,
                'dataProvider' => $dataProvider,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps102_3', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        $mpdf = new \mPDF('utf-8', 'A4-L');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Muzzam Teknologi Sdn Bhd|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $mpdf->WriteHTML($css, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output();
        exit;
    }
    public static function PdfKewps4($id) {
        $id = 21;
        $month = 11;
        $year = 2016;
        $pageSize = 30;
        // $pageSize = '';
        $page = 1;
        $inventory = Inventories::find($id)->with('category')->asArray()->one();
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \common\models\TransactionsAll::find()
            ->with('checkBy.mpspProfile')
            ->where(['INVENTORY_ID' => $id])
            ->andWhere(['extract(year from APPROVED_DATE)'=> $year])
            ->andWhere(['extract(month from APPROVED_DATE)'=> $month])
            ->orderBy(['ID'=>SORT_ASC])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            $dataProvider->pagination->page = $page;
        }
        $transactions = $dataProvider->models;
        $arraylist = array_chunk($transactions, 30);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps4_1', [
            'transactions' => $transactions,
            'inventory' => $inventory,
            ]);
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps4_2', [
                'transactions' => $list,
                'inventory' => $inventory,
                ]);
        }

        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps4_3', [
            'transactions' => $transactions,
            'inventory' => $inventory,
            ]);

        // mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right ,  $margin_top , $margin_bottom , $margin_header ,  $margin_footer , $orientation)
        $mpdf = new \mPDF('c', 'A4-P','','',15,15,16,16,9,9);
        $mpdf->SetTitle('KEW-PS 4 ('.$inventory['DESCRIPTION'].')');
        // $mpdf->default_font_size = 1;
        // $mpdf->default_font = 1;
        // $mpdf->margin_left = 15;
        // $mpdf->margin_right = 15;
        // $mpdf->margin_top = 16;
        // $mpdf->margin_bottom = 16;
        // $mpdf->margin_header = 9;
        // $mpdf->margin_footer = 9;
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Muzzam Teknologi Sdn Bhd|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Muzzam Teknologi Sdn Bhd|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $mpdf->WriteHTML($css, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 4 ('.$inventory['DESCRIPTION'].')_'.date('Y-m-d').'.pdf','I');
        exit;
        // $pdf->render();
        // exit;
    }
    public static function PdfKewps5() {
//         $model = Inventories::find();
//         $searchModel = new InventoriesSearch();
//         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//         $dataProvider->query->filterWhere(['DELETED' => 0]);
// //        $dataProvider->query->andFilterWhere(['in', 'ID', $id]);
//         $dataProvider->pagination = false;
//        // var_dump( $dataProvider->models);die();
//         $items = $dataProvider->models;
//         $content = Yii::$app->controller->renderPartial('_kewps5', [
//             'items' => $items,
//             'model' => $model,
//             'searchModel' => $searchModel,
//             'dataProvider' => $dataProvider,
//             ]);
//         $pdf = new \kartik\mpdf\Pdf([
//             'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
//             'content' => $content,
//             'marginLeft' => 15,
//             'marginRight' => 15,
//             'marginTop' => 16,
//             'marginBottom' => 16,
//             'marginHeader' => 9,
//             'marginFooter' => 9,
//             'cssFile' => '@frontend/assets/dist/css/Pdf.css',
//             'format' => 'A4',
//             'filename' => 'KEW PS 10.pdf',
//             'options' => [
//             'title' => 'Borang Pemesanan Dan Pengeluaran',
// //                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
//             ],
//             'methods' => [
//             'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
//             'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}'],
//             ]
//             ]);
//         $pdf->render();
//         exit;
        $id = 21;
        $month = 11;
        $year = 2016;
        $pageSize = 10;
        // $pageSize = '';
        $page = 1;

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \common\models\Inventories::find()
            // ->with('checkBy.mpspProfile')
            // ->where(['ID' => 0])
            // ->where(['INVENTORY_ID' => $id])
            // ->andWhere(['extract(year from APPROVED_DATE)'=> $year])
            // ->andWhere(['extract(month from APPROVED_DATE)'=> $month])
            ->orderBy(['ID'=>SORT_ASC])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            $dataProvider->pagination->page = $page;
        }


        $items = $dataProvider->models;
        // var_dump($items);die();
        $arraylist = array_chunk($items, 37);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps5_1');
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps5_2', [
                'items' => $list,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps5_3');

        // mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right ,  $margin_top , $margin_bottom , $margin_header ,  $margin_footer , $orientation)
        $mpdf = new \mPDF('c', 'A4-P','','',15,15,16,16,9,9);
        $mpdf->SetTitle('KEW-PS 4 (Borang Pemesanan Dan Pengeluaran)');
        // $mpdf->default_font_size = 1;
        // $mpdf->default_font = 1;
        // $mpdf->margin_left = 15;
        // $mpdf->margin_right = 15;
        // $mpdf->margin_top = 16;
        // $mpdf->margin_bottom = 16;
        // $mpdf->margin_header = 9;
        // $mpdf->margin_footer = 9;
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Muzzam Teknologi Sdn Bhd|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Muzzam Teknologi Sdn Bhd|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $mpdf->WriteHTML($css, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 4 (Borang Pemesanan Dan Pengeluaran)_'.date('Y-m-d').'.pdf','I');
        exit;

    }

    public static function PdfKewps7($id = [145, 144]) {
//        $model = Orders::findOne($id);
//        $searchModel = new RequestSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
//        $items = $dataProvider->models;
//        $content = Yii::$app->controller->renderPartial('_kewps7', [
//            'items' => $items,
//            'model' => $model,
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//        $model = Inventories::find();
        $searchModel = new InventoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['DELETED' => 0]);
        $dataProvider->query->andFilterWhere(['in', 'ID', $id]);
//        $dataProvider->pagination->pageSize = 40;
//        $dataProvider->pagination->params['page'] = 1;
        $items = $dataProvider->models;
        $content = Yii::$app->controller->renderPartial('_kewps7', [
            'items' => $items,
//            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
            'marginLeft' => 13,
            'marginRight' => 13,
            'marginTop' => 10,
            'marginBottom' => 6,
            'marginHeader' => 5,
            'marginFooter' => 5,
//            'marginLeft' => 15,
//            'marginRight' => 15,
//            'marginTop' => 16,
//            'marginBottom' => 16,
//            'marginHeader' => 9,
//            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
//                'SetHTMLHeaderByName' => ['header1'],
            'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}'],
            ]
            ]);
//        $mpdf = $pdf->getApi();
//        $cssName = '@frontend/assets/dist/css/Pdf.css';
//        $cssFile = Yii::getAlias($cssName);
//        $stylesheet = file_get_contents($cssFile);
//        $mpdf->DefHTMLHeaderByName('header1','<div>
//            <p class="pull-right form-lampiran"><strong>LAMPIRAN B</strong><br><strong>KEW.PS-7</strong></p>
//            <p class="text-center form-name"><strong>PENENTUAN KUMPULAN STOK</strong></p>
//            <p ><strong>Seksyen : ....</strong></p>
//        </div>');
//        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A');
//        $mpdf->SetFooter('Copyright © Muzzam Teknologi Sdn Bhd||');
//        $mpdf->WriteHTML($stylesheet, 1);
//        $mpdf->WriteHTML($content, 2);
//        $mpdf->WriteHTML($content);
//        $mpdf->Output();
//        $pdf->render();
        $pdf->render();
        exit;
    }

    public static function PdfKewps8($id = 145) {
//        $model = Inventories::findOne($id);
        $searchModel = new InventoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['DELETED' => 0]);
        $dataProvider->query->andFilterWhere(['in', 'ID', $id]);
        $items = $dataProvider->models;
        $content = Yii::$app->controller->renderPartial('_kewps8', [
            'items' => $items,
//            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
            'marginLeft' => 19,
            'marginRight' => 17,
            'marginTop' => 35,
            'marginBottom' => 6,
            'marginHeader' => 5,
            'marginFooter' => 5,
//            'marginLeft' => 15,
//            'marginRight' => 15,
//            'marginTop' => 16,
//            'marginBottom' => 16,
//            'marginHeader' => 9,
//            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 8.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
            'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||'],
            ],
            ]);

        $mpdf = $pdf->getApi();
        $cssName = '@frontend/assets/dist/css/Pdf.css';
        $isi = ''
        . '<div class="pdf"><div class="desc"><span>Hallo World</span></div>'
        . '<div class="first">'
        . '<div id="first-left"><span>Hallo WorldHallo WorldHallo Wor</span></div>'
        . '<div id="first-right"><span>Hallo World</span></div>'
        . '</div>'
        . '<div class="second">'
        . '<div id="second-left"><span>Hallo WorldHallo WorldHallo Wor</span></div>'
        . '<div id="second-right"><span>Hallo World</span></div>'
        . '</div>'
        . '<div class="third">'
        . '<div id="third-left"><span>Hallo WorldHallo WorldHallo Wor</span></div>'
        . '<div id="third-right"><span>Hallo World</span></div>'
        . '</div></div>'
        . '';
        $cssFile = Yii::getAlias($cssName);
        $stylesheet = file_get_contents($cssFile);
//        var_dump($stylesheet);die();
//        $mpdf->WriteHTML($stylesheet, 1);
//        $mpdf->WriteHTML($content, 2);
//        $fields = array(
//            'NO.KOD:' => 'My name',
//        );
        $mpdf->SetImportUse();
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A');
        $mpdf->SetFooter('Copyright © Muzzam Teknologi Sdn Bhd||');
//        $mpdf->SetDocTemplate('KEW.PS-8_FORM3.pdf', true);
//        $mpdf->mpdfform = true;
//        var_dump($mpdf);die();
//        $mpdf->Load($fields, false);
//        $mpdf->Merge();
        $mpdf->WriteHTML($stylesheet, 1);
//        $mpdf->WriteHTML($isi,2);
        $mpdf->WriteHTML($content, 2);
//        $mpdf->Thumbnail('KEW.PS-8_FORM3.pdf', 4);
        $mpdf->Output();

//        $pdf->render();
        exit;
    }

    public static function PdfKewps82($id = 2) {
        $model = Orders::findOne($id);
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
        $items = $dataProvider->models;
        $content = Yii::$app->controller->renderPartial('_kewps8', [
            'items' => $items,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
//        $pdf = new \kartik\mpdf\Pdf([
//            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
//            'content' => $content,
////            'defaultFontSize' => 30,
////            'defaultFont' => '',
////
//            'marginLeft' => 13,
//            'marginRight' => 13,
//            'marginTop' => 10,
//            'marginBottom' => 6,
//            'marginHeader' => 5,
//            'marginFooter' => 5,
////            'marginLeft' => 15,
////            'marginRight' => 15,
////            'marginTop' => 16,
////            'marginBottom' => 16,
////            'marginHeader' => 9,
////            'marginFooter' => 9,
//            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
//            'format' => 'A4-L',
//            'filename' => 'KEW PS 10.pdf',
////            'options' => [
////                'title' => 'Borang Pemesanan Dan Pengeluaran',
//////                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
////            ],
////            'methods' => [
////                'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
////                'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}'],
////            ]
//        ]);
////        $pdf = $pdf->api;
//        $mpdf = $pdf->api; // fetches mpdf api
//        $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
////        $mpdf->WriteHtml($content); // call mpdf write html
//        $mpdf->Output(); // call the mpdf api output as needed
//        $mpdf->render('filename', 'D'); // call the mpdf api output as needed
//        die();
//        $mpdf = new \mPDF('utf-8', 'A4-L');
//        $cssName = '@frontend/assets/dist/css/Pdf.css';
//        $cssFile = Yii::getAlias($cssName);
//        $stylesheet = file_get_contents($cssFile);
//        $mpdf->SetHTMLHeader($header);
//        $pdfFile = '/pdf_template/KEW.PS-8.pdf';
//        $pdfFile = '/pdf_template';
//        $pdfFile = '/uploads/profile_pic/admin1.jpg';
//        $pdfFile = '/uploads/profile_pic/admin1.jpg';
//        $pdfPath = Yii::getAlias('@web').($pdfFile);
//        $pdfPath = Yii::getAlias('@web');
//        $pdfPath = $pdfFile;
//        var_dump($pdfPath);die();
//        $pdfPath = Yii::getAlias('@frontend/assets/dist/pdf_template/KEW.PS-82.pdf');;
//        $pdfPath = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist').($pdfFile);
//        $pdfPath = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist');
//        $pdfPath = Yii::$app->assetManager->getPublishedUrl('@web');
//        var_dump($pdfPath);die();
//        $checkfile = scandir($pdfPath);
//        $checkfile = file_exists($pdfPath);
//        $checkfile = file_exists($cssFile);
//        var_dump(ini_get('directory browsing'));die();
//        var_dump(readdir(opendir($pdfPath)));die();
//        var_dump($cssFile);die();
//        var_dump($checkfile);die();
//        $pdfPath = Yii::getAlias('@frontend/assets/dist/pdf_template/KEW.PS-82.pdf');
//        $mpdf->SetImportUse();
//        $pagecount = $mpdf->SetSourceFile($pdfPath);
//        $pagecount = $mpdf->SetSourceFile('KEW.PS-8_FORM3.pdf');
//        $tplIdx = $mpdf->ImportPage($pagecount);
//        $mpdf->UseTemplate($tplIdx);
//        $mpdf->WriteHTML('1');
//        $mpdf->WriteHTML($content);
//        $mpdf->WriteHTML($stylesheet, 1);
//        $mpdf->WriteHTML($content, 2);
//        $pdf->render();
//        $mpdf->Output();
//        $mpdf = new \mPDF('utf-8', 'A4-L');
//
//        $mpdf->SetImportUse();
//        $mpdf->SetDocTemplate('KEW.PS-8_FORM3.pdf', true);
////        $mpdf->WriteHTML('Hallo World');
//
//        $mpdf->Output();
        $mpdf = new \mPDF('utf-8', 'A4-L');

//         $mpdf->WriteHTML('<!DOCTYPE  html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
//<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><title>KEW.PS-8</title><meta name="author" content="user2"/><style type="text/css"> * {margin:0; padding:0; text-indent:0; }
// p { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: bold; text-decoration: none; font-size: 12pt; margin:0pt; }
// .s1 { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: bold; text-decoration: none; font-size: 12pt; }
//</style></head><body><p style="text-indent: 0pt;text-align: left;"><br/></p><p style="padding-top: 4pt;text-indent: 0pt;text-align: right;">LAMPIRAN A</p><p style="text-indent: 0pt;text-align: left;"><br/></p><table style="border-collapse:collapse;margin-left:5.32pt" cellspacing="0"><tr style="height:105pt"><td style="width:738pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt" colspan="2"><p class="s1" style="padding-left: 674pt;text-indent: 0pt;line-height: 14pt;text-align: left;">KEW.PS-8</p><p style="text-indent: 0pt;text-align: left;"><br/></p><p class="s1" style="padding-top: 8pt;padding-left: 220pt;text-indent: 0pt;text-align: left;">LABEL MASUK-DAHULU-KELUAR DAHULU (MDKD)</p></td></tr><tr style="height:52pt"><td style="width:738pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt" colspan="2"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;line-height: 14pt;text-align: left;">PERIHAL STOK:</p></td></tr><tr style="height:51pt"><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;line-height: 14pt;text-align: left;">NO.KOD:</p></td><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;line-height: 14pt;text-align: left;">TARIKH DIBUAT:</p></td></tr><tr style="height:49pt"><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;text-align: left;">NO. LOKASI STOK:</p></td><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;text-align: left;">TARIKH LUPUT:</p></td></tr><tr style="height:52pt"><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;text-align: left;">NO. PEMBUAT/PENGENALAN:</p></td><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;text-align: left;">TARIKH DITERIMA:</p></td></tr></table></body></html>
//');
        $mpdf->WriteHTML('<style type="text/css"> * {margin:0; padding:0; text-indent:0; }
         p { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: bold; text-decoration: none; font-size: 12pt; margin:0pt; }
         .s1 { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: bold; text-decoration: none; font-size: 12pt; }
     </style>
     ', 1);
        $mpdf->WriteHTML('<p style="text-indent: 0pt;text-align: left;"><br/></p><p style="padding-top: 4pt;text-indent: 0pt;text-align: right;">LAMPIRAN A</p><p style="text-indent: 0pt;text-align: left;"><br/></p><table style="border-collapse:collapse;margin-left:5.32pt" cellspacing="0"><tr style="height:105pt"><td style="width:738pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt" colspan="2"><p class="s1" style="padding-left: 674pt;text-indent: 0pt;line-height: 14pt;text-align: left;">KEW.PS-8</p><p style="text-indent: 0pt;text-align: left;"><br/></p><p class="s1" style="padding-top: 8pt;padding-left: 220pt;text-indent: 0pt;text-align: left;">LABEL MASUK-DAHULU-KELUAR DAHULU (MDKD)</p></td></tr><tr style="height:52pt"><td style="width:738pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt" colspan="2"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;line-height: 14pt;text-align: left;">PERIHAL STOK:</p></td></tr><tr style="height:51pt"><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;line-height: 14pt;text-align: left;">NO.KOD:</p></td><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;line-height: 14pt;text-align: left;">TARIKH DIBUAT:</p></td></tr><tr style="height:49pt"><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;text-align: left;">NO. LOKASI STOK:</p></td><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;text-align: left;">TARIKH LUPUT:</p></td></tr><tr style="height:52pt"><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;text-align: left;">NO. PEMBUAT/PENGENALAN:</p></td><td style="width:369pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0pt;text-align: left;">TARIKH DITERIMA:</p></td></tr></table>
            ', 2);

        $mpdf->Output();
        exit;
    }

    public static function PdfKewps9($id = 2) {
        $searchModel = new InventoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['DELETED' => 0]);
//        $dataProvider->query->andFilterWhere(['in', 'ID', $id]);
        $items = $dataProvider->models;
        $content = Yii::$app->controller->renderPartial('_kewps9', [
            'items' => $items,
//            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
            'marginLeft' => 13,
            'marginRight' => 13,
            'marginTop' => 10,
            'marginBottom' => 6,
            'marginHeader' => 5,
            'marginFooter' => 5,
//            'marginLeft' => 15,
//            'marginRight' => 15,
//            'marginTop' => 16,
//            'marginBottom' => 16,
//            'marginHeader' => 9,
//            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
            'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}'],
            ]
            ]);
//        $mpdf = new \mPDF('utf-8', 'A4-L');
//        $cssName = '@frontend/assets/dist/css/Pdf.css';
//        $cssFile = Yii::getAlias($cssName);
//        $stylesheet = file_get_contents($cssFile);
//        $mpdf->WriteHTML($stylesheet, 1);
//        $mpdf->WriteHTML($content, 2);

        $pdf->render();
        exit;
    }

    public static function PdfKewps11($id = 86) {
        $model = Orders::findOne($id);
//        $model = Yii::$app->controller->findModel($id);
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
        $items = $dataProvider->models;
//        var_dump($items);die();
//        return Yii::$app->controller->render('_kewps11', [
//            'items' => $items,
//            'model' => $model,
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
        $content = Yii::$app->controller->renderPartial('_kewps11', [
            'items' => $items,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
//            'marginLeft' => 13,
//            'marginRight' => 13,
//            'marginTop' => 10,
//            'marginBottom' => 6,
//            'marginHeader' => 5,
//            'marginFooter' => 5,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'marginHeader' => 9,
            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
            'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}'],
            ]
            ]);
//        $mpdf = new \mPDF('utf-8', 'A4');
//        $cssName = '@frontend/assets/dist/css/Pdf.css';
//        $cssFile = Yii::getAlias($cssName);
//        $stylesheet = file_get_contents($cssFile);
//        $mpdf->WriteHTML($stylesheet, 1);
//        $mpdf->WriteHTML($content, 2);

        $pdf->render();
        exit;
    }

    public static function PdfKewps13($id = 2, $year = 2016) {
        $searchModel = new \common\models\ReportAllSearch();
        $searchModel->YEAR = $year;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
        $items = $dataProvider->models;
        $items = \yii\helpers\ArrayHelper::index($items, 'QUARTER');
//        var_dump(isset($items[2]));die();
        $content = Yii::$app->controller->renderPartial('_kewps13', [
            'year' => $year,
            'items' => $items,
//            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
//            'marginLeft' => 13,
//            'marginRight' => 13,
//            'marginTop' => 10,
//            'marginBottom' => 6,
//            'marginHeader' => 5,
//            'marginFooter' => 5,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'marginHeader' => 9,
            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran C'],
            'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}'],
            ]
            ]);
        $mpdf = new \mPDF('utf-8', 'A4-L');
        $cssName = '@frontend/assets/dist/css/Pdf.css';
        $cssFile = Yii::getAlias($cssName);
        $stylesheet = file_get_contents($cssFile);
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($content, 2);

        $pdf->render();
        exit;
    }

    public static function PdfKewps14($id = 2) {
        $model = Orders::findOne($id);
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
        $items = $dataProvider->models;
        $content = Yii::$app->controller->renderPartial('_kewps14', [
            'items' => $items,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
//            'marginLeft' => 13,
//            'marginRight' => 13,
//            'marginTop' => 10,
//            'marginBottom' => 6,
//            'marginHeader' => 5,
//            'marginFooter' => 5,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'marginHeader' => 9,
            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
            'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}'],
            ]
            ]);
        $mpdf = new \mPDF('utf-8', 'A4-L');
        $cssName = '@frontend/assets/dist/css/Pdf.css';
        $cssFile = Yii::getAlias($cssName);
        $stylesheet = file_get_contents($cssFile);
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($content, 2);

        $pdf->render();
        exit;
    }

    public static function PdfKewps17($id = 2) {
        $searchModel = new InventoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['DELETED' => 0]);
//        $dataProvider->query->andFilterWhere(['in', 'ID', $id]);
        $items = $dataProvider->models;
        $content = Yii::$app->controller->renderPartial('_kewps17', [
            'items' => $items,
//            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
//            'marginLeft' => 13,
//            'marginRight' => 13,
//            'marginTop' => 10,
//            'marginBottom' => 6,
//            'marginHeader' => 5,
//            'marginFooter' => 5,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'marginHeader' => 9,
            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran D'],
            'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}'],
            ]
            ]);
        $mpdf = new \mPDF('utf-8', 'A4-L');
        $cssName = '@frontend/assets/dist/css/Pdf.css';
        $cssFile = Yii::getAlias($cssName);
        $stylesheet = file_get_contents($cssFile);
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A');
        $mpdf->SetFooter('Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($content, 2);

        $pdf->render();
//        $mpdf->Output();
        exit;
    }

    public static function PdfKewps18($id = 2) {
        $model = Orders::findOne($id);
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
        $items = $dataProvider->models;
        $content = Yii::$app->controller->renderPartial('_kewps18', [
            'items' => $items,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
//            'marginLeft' => 13,
//            'marginRight' => 13,
//            'marginTop' => 10,
//            'marginBottom' => 6,
//            'marginHeader' => 5,
//            'marginFooter' => 5,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'marginHeader' => 9,
            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
            'SetFooter' => ['Copyright © Muzzam Teknologi Sdn Bhd||{PAGENO}'],
            ]
            ]);
        $mpdf = new \mPDF('utf-8', 'A4-L');
        $cssName = '@frontend/assets/dist/css/Pdf.css';
        $cssFile = Yii::getAlias($cssName);
        $stylesheet = file_get_contents($cssFile);
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($content, 2);

        $pdf->render();
        exit;
    }



}
