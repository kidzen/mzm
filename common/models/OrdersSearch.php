<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'number'],
            [['TRANSACTION_ID', 'APPROVED', 'APPROVED_BY', 'VEHICLE_ID', 'CHECKOUT_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['ORDER_DATE', 'ORDERED_BY', 'ORDER_NO', 'APPROVED_AT', 'REQUIRED_DATE', 'CHECKOUT_DATE', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['ORDERS.DELETED' => 0]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'TRANSACTION_ID' => $this->TRANSACTION_ID,
            'ORDERED_BY' => $this->ORDERED_BY,
            'APPROVED' => $this->APPROVED,
            'APPROVED_BY' => $this->APPROVED_BY,
            'APPROVED_AT' => $this->APPROVED_AT,
            'VEHICLE_ID' => $this->VEHICLE_ID,
            'CHECKOUT_BY' => $this->CHECKOUT_BY,
            'CREATED_AT' => $this->CREATED_AT,
            'UPDATED_AT' => $this->UPDATED_AT,
            'CREATED_BY' => $this->CREATED_BY,
            'UPDATED_BY' => $this->UPDATED_BY,
            'DELETED' => $this->DELETED,
            'DELETED_AT' => $this->DELETED_AT,
        ]);

        $query->andFilterWhere(['like', 'ORDER_DATE', $this->ORDER_DATE])
            ->andFilterWhere(['like', 'ORDER_NO', $this->ORDER_NO])
            ->andFilterWhere(['like', 'REQUIRED_DATE', $this->REQUIRED_DATE])
            ->andFilterWhere(['like', 'CHECKOUT_DATE', $this->CHECKOUT_DATE]);

        return $dataProvider;
    }
}
