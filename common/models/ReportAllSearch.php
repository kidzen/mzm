<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ReportAll;

/**
 * ReportAllSearch represents the model behind the search form about `common\models\ReportAll`.
 */
class ReportAllSearch extends ReportAll {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ID', 'YEAR', 'COUNT_IN', 'PRICE_IN', 'COUNT_OUT', 'PRICE_OUT', 'COUNT_CURRENT', 'PRICE_CURRENT'], 'number'],
            [['QUARTER'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ReportAll::find();

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//             $query->where('0=1');
            return $dataProvider;
        }
// grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'YEAR' => $this->YEAR,
            'COUNT_IN' => $this->COUNT_IN,
            'PRICE_IN' => $this->PRICE_IN,
            'COUNT_OUT' => $this->COUNT_OUT,
            'PRICE_OUT' => $this->PRICE_OUT,
            'COUNT_CURRENT' => $this->COUNT_CURRENT,
            'PRICE_CURRENT' => $this->PRICE_CURRENT,
        ]);

        $query->andFilterWhere(['like', 'QUARTER', $this->QUARTER]);

        return $dataProvider;
    }

}
