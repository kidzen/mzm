<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use common\models\InventoryItems;
use yii\web\response;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class InventoryItemsController extends ActiveController
{
    // public $modelClass = 'common\models\People';    
	public $modelClass = 'api\modules\v1\models\InventoryItems';    

	public function actions()
	{
		$actions = parent::actions();
		unset($actions['view']);
		return $actions;
	}

	public function actionView($id)
	{
		// 01090102-0916-000022
		// $data = InventoryItems::findOne($id);
		$sku = substr_replace(substr_replace($id, '-', 8,0),'-', -6,0);
		$data = InventoryItems::find()->where(['SKU'=>$sku])->with('category','orders')->asArray()->one();
		// $data = InventoryItems::find()->where(['SKU'=>$sku])->with('category')->one();
		// var_dump($data);die();
		// return $sku;
		return $data;
    // prepare and return a data provider for the "index" action
	}
}


