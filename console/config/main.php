<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
//        'db' => [
//            'class' => 'yii\db\Connection',
//            'dsn' => 'oci:dbname=//127.0.0.1:1521/ESTOR;charset=UTF8',
////            'dsn' => 'oci:dbname=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521))(CONNECT_DATA=(SID=ESTOR)));charset=UTF8;', // Oracle
//            'username' => 'estor',
//            'password' => 'estor',
////            'emulatePrepare' => TRUE,
////            'attributes' => [
//////                PDO::ATTR_EMULATE_PREPARES  => true,
//////                PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
//////                PDO::ATTR_STRINGIFY_FETCHES => true,
//////                PDO::ATTR_CASE => PDO::CASE_LOWER,
////            ],
////            'on afterOpen' => function($event) {
////                //$event->sender refers to the DB connection 
//////                $event->sender->createCommand("ALTER SESSION SET NLS_NUMERIC_CHARACTERS = ',.'")->execute();
//////                $event->sender->createCommand("ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'")->execute();
//////                $event->sender->createCommand("ALTER SESSION SET NLS_SORT = WEST_EUROPEAN")->execute();
////},
////            'enableSchemaCache' => true,
//            // Duration of schema cache.
////            'schemaCacheDuration' => 3600,
////            'schemaCacheDuration' => 100,
//// Name of the cache component used to store schema information
////            'schemaCache' => 'cache',
//        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
//        'db' => [
//            'class' => 'yii\db\Connection',
//            'dsn' => 'mysql:host=localhost;dbname=estor',
//            'username' => 'root',
//            'password' => '',
//            'charset' => 'utf8',
//            'enableSchemaCache' => true,
//        ],
//         'db' => [
//             'class' => 'yii\db\Connection',
//             'dsn' => 'oci:dbname=//localhost:1521/mpsp12c;charset=UTF8',
// //            'dsn' => 'mysql:host=localhost;dbname=estor',
// //            'dsn' => 'oci:dbname=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521))(CONNECT_DATA=(SID=ESTOR)));charset=UTF8;', // Oracle
//             'username' => 'estor2',
//             'password' => 'estor',
// //            'charset' => 'utf8',
// //            'enableSchemaCache' => true,
//         ],
    ],
    'params' => $params,
];
