<?php

use yii\db\Migration;

class m130524_201442_init extends Migration {

    public function up() {
        //db oracle
        if(Yii::$app->db->driverName == 'oci'){                
            foreach (array_filter(array_map('trim', explode(';', $this->getSchemaSql()))) as $query) {
                $this->execute($query);
            }


        }
        //db mysql

        if(Yii::$app->db->driverName == 'mysql'){
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }
            // people
            $this->createTable('{{%people}}', [
                'id' => $this->primaryKey(),
                'username' => $this->string()->unique(),
                'email' => $this->string(),
                'staff_no' => $this->string(),
                'profile_pic' => $this->string(),
                'password' => $this->string(),
                'role_id' => $this->integer()->notNull(),
                'password_reset_token' => $this->string()->unique(),
                'auth_key' => $this->string(32),

                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->integer(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                ], $tableOptions);
            // activity_log
            $this->createTable('{{%activity_log}}', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(),
                'remote_ip' => $this->string(),
                'action' => $this->string(),
                'controller' => $this->string(),
                'params' => $this->string(),
                'route' => $this->string(),
                'status' => $this->string(),
                'messages' => $this->string(),
                'created_at' => $this->timestamp(),
                ], $tableOptions);
            // categories
            $this->createTable('{{%categories}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // inventories
            $this->createTable('{{%inventories}}', [
                'id' => $this->primaryKey(),
                'category_id' => $this->integer(),
                'card_no' => $this->string(),
                'code_no' => $this->string(),
                'description' => $this->string(),
                'quantity' => $this->integer(),
                'min_stock' => $this->integer(),
                'location' => $this->string(),
                'approved' => $this->smallinteger(),
                'approved_at' => $this->timestamp(),
                'approved_by' => $this->integer(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // inventories_checkin
            $this->createTable('{{%inventories_checkin}}', [
                'id' => $this->primaryKey(),
                'transaction_id' => $this->integer(),
                'inventory_id' => $this->integer(),
                'vendor_id' => $this->integer(),
                'items_quantity' => $this->integer(),
                'items_total_price' => $this->float(),
                'check_date' => $this->date(),
                'check_by' => $this->integer(),
                'approved' => $this->smallinteger(),
                'approved_at' => $this->timestamp(),
                'approved_by' => $this->integer(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // inventory_items
            $this->createTable('{{%inventory_items}}', [
                'id' => $this->primaryKey(),
                'inventory_id' => $this->integer(),
                'checkin_transaction_id' => $this->integer(),
                'checkout_transaction_id' => $this->integer(),
                'sku' => $this->string(),
                'unit_price' => $this->decimal(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // orders
            $this->createTable('{{%orders}}', [
                'id' => $this->primaryKey(),
                'transaction_id' => $this->integer(),
                'arahan_kerja_id' => $this->integer(),
                'order_date' => $this->date(),
                'ordered_by' => $this->string(),
                'order_no' => $this->string(),
                'vehicle_id' => $this->integer(),
                'required_date' => $this->date(),
                'checkout_date' => $this->date(),
                'checkout_by' => $this->integer(),
                'approved' => $this->smallinteger(),
                'approved_at' => $this->timestamp(),
                'approved_by' => $this->integer(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // order_items
            $this->createTable('{{%order_items}}', [
                'id' => $this->primaryKey(),
                'inventory_id' => $this->integer(),
                'order_id' => $this->integer(),
                'rq_quantity' => $this->integer(),
                'app_quantity' => $this->integer(),
                'current_balance' => $this->integer(),
                'unit_price' => $this->float(),
                'string' => $this->string(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // package
            $this->createTable('{{%package}}', [
                'id' => $this->primaryKey(),
                'order_id' => $this->integer(),
                'detail' => $this->string(),
                'delivery' => $this->string(),
                'package_by' => $this->integer(),
                'package_date' => $this->date(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // roles
            $this->createTable('{{%roles}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // transactions
            $this->createTable('{{%transactions}}', [
                'id' => $this->primaryKey(),
                'type' => $this->integer(),
                'check_date' => $this->date(),
                'check_by' => $this->integer(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // usage_list
            $this->createTable('{{%usage_list}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // vehicle_list
            $this->createTable('{{%vehicle_list}}', [
                'id' => $this->primaryKey(),
                'reg_no' => $this->string(),
                'model' => $this->string(),
                'type' => $this->string(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);
            // vendors
            $this->createTable('{{%vendors}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(),
                'address' => $this->string(),
                'contact_no' => $this->string(),
                'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
                ], $tableOptions);

            //create view using sql callback
            

        }
    }

    private function getSchemaSql() {
        if(Yii::$app->db->driverName == 'oci'){                
            $fileName = 'schema-v3.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);

            $sql = $commandR;
            return "$sql";
        }
    }

    public function down() {
        // db oracle
        if(Yii::$app->db->driverName == 'oci'){                
            try {
                $sqlObject = Yii::$app->db->createCommand("select 'drop table ' || table_name || ' cascade constraints;' as sql from user_tables")->queryAll();
                foreach ($sqlObject as $query) {
                    var_dump($query['SQL']);
                    $command = Yii::$app->db->createCommand($query['SQL']);
                    $command->execute();
                }

                $sqlObject = Yii::$app->db->createCommand("select 'drop table ' || view_name || ' cascade constraints;' as sql from user_views; ")->queryAll();
                foreach ($sqlObject as $query) {
                    var_dump($query['SQL']);
                    $command = Yii::$app->db->createCommand($query['SQL']);
                    $command->execute();
                }
            } catch (\yii\db\Exception $e) {

            }
        }
        //db mysql
        if(Yii::$app->db->driverName == 'mysql'){                
            // all FKs
            // $this->dropForeignKey('fk_profile_user_id','{{profile}}');
            // $this->dropForeignKey('fk_user_role_id','{{user}}');
            // people
            $this->dropTable('{{%people}}');
            // activity_log
            $this->dropTable('{{%activity_log}}');
            // categories
            $this->dropTable('{{%categories}}');
            // inventories
            $this->dropTable('{{%inventories}}');
            // inventories_checkin
            $this->dropTable('{{%inventories_checkin}}');
            // inventory_items
            $this->dropTable('{{%inventory_items}}');
            // orders
            $this->dropTable('{{%orders}}');
            // order_items
            $this->dropTable('{{%order_items}}');
            // package
            $this->dropTable('{{%package}}');
            // roles
            $this->dropTable('{{%roles}}');
            // transactions
            $this->dropTable('{{%transactions}}');
            // usage_list
            $this->dropTable('{{%usage_list}}');
            // vehicle_list
            $this->dropTable('{{%vehicle_list}}');
            // vendors
            $this->dropTable('{{%vendors}}');
        }
    }

}
