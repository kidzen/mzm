<?php

use yii\db\Migration;

class m161009_215038_data extends Migration
{
    public function up() {
        //db oracle
        if(Yii::$app->db->driverName == 'oci'){                
            foreach (array_filter(array_map('trim', explode(';', $this->getSchemaSql()))) as $query) {
                $this->execute($query);
            }

        }
        if(Yii::$app->db->driverName == 'mysql'){                
            echo "No data";
        }
    }

    private function getSchemaSql() {
        // db oracle
        if(Yii::$app->db->driverName == 'oci'){                
            $fileName = 'data-v3.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);

            $sql = $commandR;
            return "$sql";
        }
    }

    public function down() {
        // db oracle
        if(Yii::$app->db->driverName == 'oci'){                

            try {
                $sqlObject = Yii::$app->db->createCommand("select 'truncate table ' || table_name || ' ;' as sql from user_tables")->queryAll();
                foreach ($sqlObject as $query) {
                    var_dump($query['SQL']);
                    $command = Yii::$app->db->createCommand($query['SQL']);
                    $command->execute();
                }

            } catch (\yii\db\Exception $e) {

            }
        }
        //db mysql
        if(Yii::$app->db->driverName == 'mysql'){                
            echo "No data";
        }
    }
}
